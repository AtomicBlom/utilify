﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawing=System.Drawing;
using System.Text;
using System.Windows.Forms;
using RayTracer;
using Utilify.Framework.UI;
using Utilify.Platform.Configuration;
using Utilify.Framework;
using System.Net;

namespace Utilify.Platform.Demo.UtilifyRayTracer
{
    public partial class UtilifyRayTracer : Form
    {
        private Scene scene = new Scene();
        private AntiAliasing antiAliasing = AntiAliasing.Medium;
        private Drawing.Bitmap localBitmap;
        private Drawing.Bitmap utilifyBitmap;
        private Utilify.Framework.Application app;
        private System.Threading.Thread localThread;
        private Utilify.Platform.Logger logger;
        private DateTime utilifyStartTime;
        private IList<Job> utilifyJobs;
        private ConnectionSettings connectionSettings;
        private bool isUtilifyTracing = false;
        private bool isLocalTracing = false;
        
        public UtilifyRayTracer()
        {
            logger = new Utilify.Platform.Logger();
            logger.Debug("Initialising...");

            InitializeComponent();

            localBitmap = new Drawing.Bitmap(400, 400);
            pbSceneLocal.Image = localBitmap;
            utilifyBitmap = new Drawing.Bitmap(400, 400);
            pbSceneUtilify.Image = utilifyBitmap;

            // Set up the scene
            SetupScene((int)this.ballsUpDown.Value);
        }

        #region Setup scene

        private void SetupScene(int numberOfBalls)
        {
            scene = new Scene();
            scene.Background = new Background(new Color(0.0, 0.0, 0.0), 0.1);//new Background(new Color(.2, .3, .4), 0.5);
            Vector campos = new Vector(0, 0, -5);
            scene.Camera = new Camera(campos, campos / -2, new Vector(0, 1, 0).Normalize());

            Random rnd = new Random();
            for (int i = 0; i < numberOfBalls; i++)
            {

                // setup a solid reflecting sphere
                scene.Shapes.Add(new SphereShape(new Vector(rnd.Next(-100, 100) / 50.0, rnd.Next(-100, 100) / 50.0, rnd.Next(0, 200) / 50.0), .2,
                                   new SolidMaterial(new Color(rnd.Next(0, 100) / 100.0, rnd.Next(0, 100) / 100.0, rnd.Next(0, 100) / 100.0), 0.4, 0.0, 2.0)));

            }

            // setup the chessboard floor
            scene.Shapes.Add(new PlaneShape(new Vector(0.1, 0.9, -0.5).Normalize(), 1.2,
                               new ChessboardMaterial(new Color(1, 1, 1), new Color(0, 0, 0), 0.2, 0, 1, 0.7)));

            scene.Lights.Add(new Light(new Vector(5, 10, -1), new Color(0.8, 0.8, 0.8)));
            scene.Lights.Add(new Light(new Vector(-3, 5, -15), new Color(0.8, 0.8, 0.8)));
        }

        #endregion

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutUtilifyRayTracerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ray Tracing Demonstrator for the Utilify Platform.","Utilify Ray Tracer");
        }

        #region Anti Aliasing Options

        private void CheckMenuAntiAliasing(object menuitem)
        {
            noneToolStripMenuItem.Checked = noneToolStripMenuItem.Equals(menuitem);
            quickToolStripMenuItem.Checked = quickToolStripMenuItem.Equals(menuitem);
            lowToolStripMenuItem.Checked = lowToolStripMenuItem.Equals(menuitem);
            mediumToolStripMenuItem.Checked = mediumToolStripMenuItem.Equals(menuitem);
            highToolStripMenuItem.Checked = highToolStripMenuItem.Equals(menuitem);
            veryHighToolStripMenuItem.Checked = veryHighToolStripMenuItem.Equals(menuitem);
        }

        private void noneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.None;
        }

        private void quickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.Quick;
        }

        private void lowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.Low;
        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.Medium;
        }

        private void highToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.High;
        }

        private void veryHighToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckMenuAntiAliasing(sender);
            antiAliasing = AntiAliasing.VeryHigh;
        }

        #endregion

        #region Utilify Render

        private bool ShowConnectionSettings()
        {
            bool isSettingsAccepted = false;

            // Create Username Credentials to be used during AuthN and AuthZ
            LoginForm login = new LoginForm();
            DialogResult result = login.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                connectionSettings = login.ConnectionSettings;
                isSettingsAccepted = true;
            }
            else
            {
                connectionSettings = null;
            }

            return isSettingsAccepted;
        }

        private void utilifyRender_Click(object sender, EventArgs e)
        {
            // First check if we are Cancelling or Starting.
            if (isUtilifyTracing) // Cancelling
            {
                DoCancelUtilifyApp();
                return;
            }

            lblStatusUtilify.Text = "Utilify - Initialising...";
            lblStatusJobs.Text = "";

            // Clear display
            Drawing.Graphics gr = Drawing.Graphics.FromImage(pbSceneUtilify.Image);
            gr.FillRectangle(new Drawing.SolidBrush(Drawing.Color.Black), 0.0F, 0.0F, (float)pbSceneUtilify.Image.Width, (float)pbSceneUtilify.Image.Height);
            this.Refresh();

            // Create Jobs list to track Job statuses
            utilifyJobs = new List<Job>();

            if (connectionSettings == null)
            {
                if (!ShowConnectionSettings())
                {
                    lblStatusUtilify.Text = "Utilify - Connection settings must be set.";
                    return;
                }
            }

            lblStatusUtilify.Text = "Utilify - Creating Jobs...";

            // Create the Application
            IEnumerable<DependencyInfo> dependencies = DependencyResolver.DetectDependencies(typeof(UtilifyRayTracerJob));
            app = new Utilify.Framework.Application("Utilify Ray Tracer Demo " + DateTime.UtcNow.Ticks, 
                QosInfo.GetDefault(), dependencies, connectionSettings);
            app.AutoDetectDependencies = false;

            //app.Add
            // Create viewport
            Drawing.Rectangle rect = new Drawing.Rectangle(0, 0, 400, 400);

            int numRows = (int) this.rowsUpDown.Value;
            int rowHeight = (int) rect.Height / numRows;

            for (int y = 0; y < numRows; y++)
            {
                UtilifyRayTracerJob job = new UtilifyRayTracerJob(this.scene, rect, y * rowHeight, rowHeight);
                job.AntiAliasing = this.antiAliasing;
                job.RenderDiffuse = true;
                job.RenderHighlights = phongHighlightsToolStripMenuItem.Checked;
                job.RenderReflections = reflectionsToolStripMenuItem.Checked;
                job.RenderRefractions = refractionToolStripMenuItem.Checked;
                job.CastShadows = castShadowsToolStripMenuItem.Checked;

                Job remoteJob = app.AddJob(job);
                utilifyJobs.Add(remoteJob);
            }

            app.Error += new EventHandler<ErrorEventArgs>(app_Error);
            app.JobStatusChanged += new EventHandler<StatusChangedEventArgs>(app_JobStatusChanged);
            app.JobSubmitted += new EventHandler<JobSubmittedEventArgs>(app_JobSubmitted);
            app.JobCompleted += new EventHandler<JobCompletedEventArgs>(app_JobCompleted);

            utilifyStartTime = DateTime.Now;

            try
            {
                app.Start();
                lblStatusUtilify.Text = "Utilify - Application Started";
                btnUtilifyRender.Text = "Cancel";
                isUtilifyTracing = true;

            } catch (Exception ex) {
                MessageBox.Show("Problem Contacting Utilify Manager.\n" + ex.Message + "\nException: " + ex.GetType());
                lblStatusUtilify.Text = "Utilify - Problem Contacting Utilify Manager";
            }
        }

        private void DoCancelUtilifyApp()
        {
            app.Cancel();
            lblStatusUtilify.Text = "Utilify - Cancelled.";
            lblStatusJobs.Text = "";
            btnUtilifyRender.Text = "Utilify Render";
            isUtilifyTracing = false;
            this.Refresh();
        }

        void app_JobCompleted(object sender, JobCompletedEventArgs e)
        {
            if (e.Job.Status == JobStatus.Completed)
            {
                Drawing.Image result = ((UtilifyRayTracerJob) e.Job.CompletedInstance).Image;
                logger.Debug("Updating Utilify Image... ({0}) {1}", (result.Height + "x" + result.Width), e.Job.Id);
                UpdateUtilifyBitmap((UtilifyRayTracerJob)e.Job.CompletedInstance);
                UpdateUtilifyStatus();
            }
        }

        private void UpdateUtilifyBitmap(UtilifyRayTracerJob utilifyRayTracerJob)
        {
            Drawing.Graphics g = Drawing.Graphics.FromImage(pbSceneUtilify.Image);
            g.DrawImage(utilifyRayTracerJob.Image, 0, utilifyRayTracerJob.StartRow);
            pbSceneUtilify.Invalidate(new Drawing.Rectangle(0, utilifyRayTracerJob.StartRow, utilifyRayTracerJob.Image.Width, utilifyRayTracerJob.Image.Height));
            pbSceneUtilify.Refresh();
        }

        private void UpdateUtilifyStatus()
        {
            double duration = DateTime.Now.Subtract(utilifyStartTime).TotalMilliseconds;
            int completedJobs = 0;
            int executingJobs = 0;
            int waitingJobs = 0;

            foreach (Job j in utilifyJobs)
            {
                if (j.Status == JobStatus.Completed || j.Status == JobStatus.Cancelled)
                {
                    completedJobs++;
                }
                else if (j.Status == JobStatus.Completing || j.Status == JobStatus.Executing)
                {
                    executingJobs++;
                }
                else
                {
                    waitingJobs++;
                }
            }

            double progress = completedJobs / utilifyJobs.Count;
            if (progress == 0)
                progress = 0.05;
            double ETA = duration / progress - duration;

            this.Invoke((MethodInvoker)delegate()
            {
                lblStatusUtilify.Text = String.Format("Utilify - Duration: {0}s ", (duration / 1000).ToString("0.0"));
                if (completedJobs == utilifyJobs.Count)
                {
                    // All Jobs completed, so finish up.
                    // How to clean up the completed App?
                    app = null;
                    lblStatusJobs.Text = "Ray Tracing Complete!";
                    btnUtilifyRender.Text = "Utilify Render";
                    isUtilifyTracing = false;
                }
                else
                {
                    lblStatusJobs.Text = String.Format("{0} Jobs Currently Executing | {1} Jobs Waiting | {2}/{3} Jobs Completed ", executingJobs, waitingJobs, completedJobs, utilifyJobs.Count);
                }
            });

            lblStatusUtilify.Invalidate();
            lblStatusUtilify.Refresh();
        }

        void app_JobSubmitted(object sender, JobSubmittedEventArgs e)
        {
            logger.Debug("Job {0} Submitted.", e.Job.Id);
        }

        void app_JobStatusChanged(object sender, StatusChangedEventArgs e)
        {
            logger.Debug("Job {0} Status changed {1} .", e.Status.JobId, e.Status.Status);
            UpdateUtilifyStatus();
        }

        void app_Error(object sender, ErrorEventArgs e)
        {
            logger.Debug("Error occured while executing Application.");
        }

        #endregion

        #region Local Render

        private void localRender_Click(object sender, EventArgs e)
        {
            if (isLocalTracing)
            {
                DoCancelLocalApp();
                return;
            }

            localThread = new System.Threading.Thread(LocalRender);
            localThread.Start();
            btnLocalRender.Text = "Cancel";
            isLocalTracing = true;
        }

        private void DoCancelLocalApp()
        {
            localThread.Abort();
            lblStatusLocal.Text = "Local - Cancelled.";
            btnLocalRender.Text = "Local Render";
            isLocalTracing = false;
            this.Refresh();
        }

        private void LocalRender()
        {
            // Clear display
            Drawing.Graphics gr = Drawing.Graphics.FromImage(pbSceneLocal.Image);
            gr.FillRectangle(new Drawing.SolidBrush(Drawing.Color.Black), 0.0F, 0.0F, (float)pbSceneLocal.Image.Width, (float)pbSceneLocal.Image.Height);

            RayTracer.RayTracer raytracer = new RayTracer.RayTracer(antiAliasing,
                                                        true,
                                                        phongHighlightsToolStripMenuItem.Checked,
                                                        castShadowsToolStripMenuItem.Checked,
                                                        reflectionsToolStripMenuItem.Checked,
                                                        refractionToolStripMenuItem.Checked);

            raytracer.RenderUpdate += new RenderUpdateDelegate(raytracer_RenderUpdate);
            Drawing.Rectangle rect = new Drawing.Rectangle(0, 0, 400, 400);
            localBitmap = new Drawing.Bitmap(rect.Width, rect.Height);
            Drawing.Graphics g = Drawing.Graphics.FromImage(localBitmap);
            pbSceneLocal.Image = localBitmap;
            raytracer.RayTraceScene(g, rect, scene);

            this.Invoke((MethodInvoker)delegate()
            {
                btnLocalRender.Text = "Local Render";
                isLocalTracing = false;
            });
        }

        void raytracer_RenderUpdate(int progress, double duration, double ETA, int scanline)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                //only invalidate part of the picturebox that needs to be redrawn
                pbSceneLocal.Invalidate(new Drawing.Rectangle(0, scanline - 1, pbSceneLocal.Image.Width, 2));
                if (ETA > 0)
                {
                    lblStatusLocal.Text = String.Format("Local - Duration: {0}s | ETA: {1}s", (duration / 1000).ToString("0.0"), (ETA / 1000).ToString("0.0"));
                }
                else // Render Complete
                {
                    lblStatusLocal.Text = String.Format("Local - Duration: {0}s", (duration / 1000).ToString("0.0"));
                }
                System.Windows.Forms.Application.DoEvents(); // some time to redraw the screen
            });
        }

        #endregion

        private void connectionSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowConnectionSettings();
        }

        private void ballsUpDown_ValueChanged(object sender, EventArgs e)
        {
            SetupScene((int)this.ballsUpDown.Value);
        }

        private void btnRenderAll_Click(object sender, EventArgs e)
        {
            //kick off both
            utilifyRender_Click(sender, e);
            localRender_Click(sender, e);
        }
    }
}
