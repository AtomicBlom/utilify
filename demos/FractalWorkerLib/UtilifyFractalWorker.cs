using System;
using System.Collections.Generic;
using System.Text;
using Utilify.Framework;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Utilify.Platform.Demo.FractalGenerator.Worker
{
    [Serializable]
    public class UtilifyFractalWorker : LocalFractalWorker, IExecutable
    {
        public UtilifyFractalWorker(
            int xSegment, int ySegment, int segmentWidth, int segmentHeight, int xOffset, int yOffset, int zoom, int iterations, ColorBlend blend) :
            base(xSegment, ySegment, segmentWidth, segmentHeight, xOffset, yOffset, zoom, iterations, blend)
        { }

        public void Execute(ExecutionContext context)
        {
            Console.WriteLine(String.Format("Generating Fractal Segment {0},{1}.", XSegmentNum, YSegmentNum));
            this.Generate();
            Console.WriteLine(String.Format("Completed Generating Segment {0},{1}.", XSegmentNum, YSegmentNum));

        }
    }
}
