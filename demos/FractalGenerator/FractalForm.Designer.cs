namespace Utilify.Platform.Demo.FractalGenerator
{
    partial class FractalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.PictureBox remoteDisplay;
        private System.Windows.Forms.PictureBox pbColour;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbColour = new System.Windows.Forms.PictureBox();
            this.tbXSegments = new System.Windows.Forms.TextBox();
            this.tbYSegments = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnZoomOut = new System.Windows.Forms.Button();
            this.btnZoomIn = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.earliestTimePicker = new System.Windows.Forms.DateTimePicker();
            this.latestTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAbort = new System.Windows.Forms.Button();
            this.remoteDisplay = new System.Windows.Forms.PictureBox();
            this.localDisplay = new System.Windows.Forms.PictureBox();
            this.chkRunLocal = new System.Windows.Forms.CheckBox();
            this.chkRunRemote = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.remoteDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localDisplay)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox1.Controls.Add(this.pbColour);
            this.groupBox1.Location = new System.Drawing.Point(-2, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(68, 114);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Palette";
            // 
            // pbColour
            // 
            this.pbColour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbColour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbColour.Location = new System.Drawing.Point(6, 19);
            this.pbColour.Name = "pbColour";
            this.pbColour.Size = new System.Drawing.Size(56, 76);
            this.pbColour.TabIndex = 2;
            this.pbColour.TabStop = false;
            this.pbColour.Click += new System.EventHandler(this.pbColour_Click);
            // 
            // tbXSegments
            // 
            this.tbXSegments.Location = new System.Drawing.Point(6, 25);
            this.tbXSegments.Name = "tbXSegments";
            this.tbXSegments.Size = new System.Drawing.Size(34, 20);
            this.tbXSegments.TabIndex = 12;
            // 
            // tbYSegments
            // 
            this.tbYSegments.Location = new System.Drawing.Point(6, 51);
            this.tbYSegments.Name = "tbYSegments";
            this.tbYSegments.Size = new System.Drawing.Size(34, 20);
            this.tbYSegments.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tbXSegments);
            this.groupBox2.Controls.Add(this.tbYSegments);
            this.groupBox2.Location = new System.Drawing.Point(72, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(90, 114);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Segments";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Width";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Height";
            // 
            // btnDown
            // 
            this.btnDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDown.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_5;
            this.btnDown.Location = new System.Drawing.Point(723, 76);
            this.btnDown.Margin = new System.Windows.Forms.Padding(0);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(30, 30);
            this.btnDown.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btnDown, "Down");
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnRight
            // 
            this.btnRight.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnRight.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_7;
            this.btnRight.Location = new System.Drawing.Point(753, 46);
            this.btnRight.Margin = new System.Windows.Forms.Padding(0);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(30, 30);
            this.btnRight.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btnRight, "Right");
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnLeft.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_8;
            this.btnLeft.Location = new System.Drawing.Point(693, 46);
            this.btnLeft.Margin = new System.Windows.Forms.Padding(0);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(30, 30);
            this.btnLeft.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btnLeft, "Left");
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnUp.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_6;
            this.btnUp.Location = new System.Drawing.Point(723, 16);
            this.btnUp.Margin = new System.Windows.Forms.Padding(0);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(30, 30);
            this.btnUp.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btnUp, "Up");
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnZoomOut.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_11;
            this.btnZoomOut.Location = new System.Drawing.Point(693, 76);
            this.btnZoomOut.Margin = new System.Windows.Forms.Padding(0);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(30, 30);
            this.btnZoomOut.TabIndex = 7;
            this.toolTip1.SetToolTip(this.btnZoomOut, "Zoom Out");
            this.btnZoomOut.UseVisualStyleBackColor = true;
            this.btnZoomOut.Click += new System.EventHandler(this.btnZoomOut_Click);
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnZoomIn.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_10;
            this.btnZoomIn.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnZoomIn.Location = new System.Drawing.Point(693, 16);
            this.btnZoomIn.Margin = new System.Windows.Forms.Padding(0);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(30, 30);
            this.btnZoomIn.TabIndex = 6;
            this.toolTip1.SetToolTip(this.btnZoomIn, "Zoom In");
            this.btnZoomIn.UseVisualStyleBackColor = true;
            this.btnZoomIn.Click += new System.EventHandler(this.btnZoomIn_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnReset.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_20;
            this.btnReset.Location = new System.Drawing.Point(753, 16);
            this.btnReset.Margin = new System.Windows.Forms.Padding(0);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(30, 30);
            this.btnReset.TabIndex = 5;
            this.toolTip1.SetToolTip(this.btnReset, "Reset Settings");
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGenerate.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_18;
            this.btnGenerate.Location = new System.Drawing.Point(723, 46);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(30, 30);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Tag = "Generate";
            this.toolTip1.SetToolTip(this.btnGenerate, "Generate Fractal!");
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // earliestTimePicker
            // 
            this.earliestTimePicker.CustomFormat = "dd/MM/yyyy - hh:mm.ss tt";
            this.earliestTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.earliestTimePicker.Location = new System.Drawing.Point(6, 41);
            this.earliestTimePicker.Name = "earliestTimePicker";
            this.earliestTimePicker.Size = new System.Drawing.Size(153, 20);
            this.earliestTimePicker.TabIndex = 15;
            // 
            // latestTimePicker
            // 
            this.latestTimePicker.CustomFormat = "dd/MM/yyyy - hh:mm.ss tt";
            this.latestTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.latestTimePicker.Location = new System.Drawing.Point(6, 84);
            this.latestTimePicker.Name = "latestTimePicker";
            this.latestTimePicker.Size = new System.Drawing.Size(153, 20);
            this.latestTimePicker.TabIndex = 16;
            this.latestTimePicker.Value = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.earliestTimePicker);
            this.groupBox3.Controls.Add(this.latestTimePicker);
            this.groupBox3.Location = new System.Drawing.Point(168, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(166, 114);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Start Time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Latest";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Earliest";
            // 
            // btnAbort
            // 
            this.btnAbort.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnAbort.BackgroundImage = global::Utilify.Platform.Demo.FractalGenerator.Properties.Resources.greyscale_12;
            this.btnAbort.Location = new System.Drawing.Point(753, 76);
            this.btnAbort.Margin = new System.Windows.Forms.Padding(0);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(30, 30);
            this.btnAbort.TabIndex = 18;
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // remoteDisplay
            // 
            this.remoteDisplay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.remoteDisplay.BackColor = System.Drawing.Color.Black;
            this.remoteDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.remoteDisplay.Location = new System.Drawing.Point(413, 6);
            this.remoteDisplay.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.remoteDisplay.Name = "remoteDisplay";
            this.remoteDisplay.Size = new System.Drawing.Size(400, 436);
            this.remoteDisplay.TabIndex = 1;
            this.remoteDisplay.TabStop = false;
            // 
            // localDisplay
            // 
            this.localDisplay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.localDisplay.BackColor = System.Drawing.Color.Black;
            this.localDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.localDisplay.Location = new System.Drawing.Point(6, 6);
            this.localDisplay.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.localDisplay.Name = "localDisplay";
            this.localDisplay.Size = new System.Drawing.Size(400, 436);
            this.localDisplay.TabIndex = 0;
            this.localDisplay.TabStop = false;
            this.localDisplay.MouseDown += new System.Windows.Forms.MouseEventHandler(this.localDisplay_MouseDown);
            this.localDisplay.MouseUp += new System.Windows.Forms.MouseEventHandler(this.localDisplay_MouseUp);
            // 
            // chkRunLocal
            // 
            this.chkRunLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkRunLocal.AutoSize = true;
            this.chkRunLocal.Checked = true;
            this.chkRunLocal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRunLocal.Location = new System.Drawing.Point(358, 17);
            this.chkRunLocal.Name = "chkRunLocal";
            this.chkRunLocal.Size = new System.Drawing.Size(71, 17);
            this.chkRunLocal.TabIndex = 19;
            this.chkRunLocal.Text = "Run local";
            this.chkRunLocal.UseVisualStyleBackColor = true;
            // 
            // chkRunRemote
            // 
            this.chkRunRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkRunRemote.AutoSize = true;
            this.chkRunRemote.Checked = true;
            this.chkRunRemote.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRunRemote.Location = new System.Drawing.Point(358, 40);
            this.chkRunRemote.Name = "chkRunRemote";
            this.chkRunRemote.Size = new System.Drawing.Size(81, 17);
            this.chkRunRemote.TabIndex = 20;
            this.chkRunRemote.Text = "Run remote";
            this.chkRunRemote.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.chkRunRemote);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.chkRunLocal);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnAbort);
            this.panel1.Controls.Add(this.btnZoomIn);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.btnZoomOut);
            this.panel1.Controls.Add(this.btnUp);
            this.panel1.Controls.Add(this.btnDown);
            this.panel1.Controls.Add(this.btnLeft);
            this.panel1.Controls.Add(this.btnRight);
            this.panel1.Location = new System.Drawing.Point(6, 446);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 126);
            this.panel1.TabIndex = 22;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 579);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(822, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.AutoSize = false;
            this.statusBarLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(785, 17);
            this.statusBarLabel.Spring = true;
            // 
            // FractalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 601);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.remoteDisplay);
            this.Controls.Add(this.localDisplay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FractalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fractal Demonstrator";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.remoteDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localDisplay)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.PictureBox localDisplay;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnZoomIn;
        private System.Windows.Forms.Button btnZoomOut;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.TextBox tbXSegments;
        private System.Windows.Forms.TextBox tbYSegments;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DateTimePicker earliestTimePicker;
        private System.Windows.Forms.DateTimePicker latestTimePicker;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.CheckBox chkRunLocal;
        private System.Windows.Forms.CheckBox chkRunRemote;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;

    }
}

