using System;
using System.IO;
using Utilify.Platform;

namespace Utilify.Platform.Executor
{
    //Used for setting a job's jobDirectory, appDirectory on the Executor
    internal static class PathHelper
    {
        //todoConfig: should this be configurable?
        //private static String applicationDir = Path.Combine("Utilify", "Applications");

        //don't create it yet. the caller will take care of that.
        private static String appDataPathBase = Path.Combine(IOHelper.GetTempDirectory(false), "Applications");

        public static String GetApplicationDirectory(String applicationId)
        {
            return Path.Combine(appDataPathBase, applicationId);
        }

        public static String GetJobDirectory(String applicationId, String jobId)
        {
            return Path.Combine(Path.Combine(appDataPathBase, applicationId), jobId);
        }
    }
}
