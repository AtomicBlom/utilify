﻿using System;
using System.IO;

namespace Utilify.Platform.Executor
{
    interface IResultUploader
    {
        bool PutResult(string jobId, string localPath, ResultInfo resultInfo);
    }

    abstract class ResultUploader<T> : IResultUploader where T : IManagerService
    {
        protected Logger logger = new Logger();
        protected T proxy;

        protected ResultUploader(T proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException(Errors.ValueCannotBeNull, "proxy");

            this.proxy = proxy;
        }

        public abstract bool PutResult(string jobId, string localPath, ResultInfo resultInfo);
    }

    class RemoteResultUploader : ResultUploader<IDataTransferService>
    {
        internal RemoteResultUploader(IDataTransferService proxy)
            : base(proxy)
        {
        }

        public override bool PutResult(string jobId, string localPath, ResultInfo resultInfo)
        {
            if (resultInfo == null || string.IsNullOrEmpty(resultInfo.FileName))
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "resultInfo");
            }

            if (resultInfo.RetrievalMode != ResultRetrievalMode.StoreRemotely)
            {
                throw new ArgumentException(string.Format("Invalid result type: {0}", resultInfo.RetrievalMode), "resultInfo");
            }
            try
            {
                DataTransferInfo data = new DataTransferInfo(localPath);
                //set the object name explicitly to a unique name on the remote server
                data.ObjectName = Path.GetFileNameWithoutExtension(string.Format("{0}_{1}.{2}", localPath, jobId, Path.GetExtension(localPath)));
                data.Overwrite = true;
                proxy.Put(data);
                return true;
            }
            catch (IOException ix)
            {
                logger.Warn("Error sending result: ", ix);
            }

            return false;
        }
    }

    class SimpleResultUploader : ResultUploader<IJobManager>
    {
        internal SimpleResultUploader(IJobManager proxy)
            : base(proxy)
        {
        }

        public override bool PutResult(string jobId, string localPath, ResultInfo resultInfo)
        {
            if (resultInfo == null || string.IsNullOrEmpty(resultInfo.FileName))
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "resultInfo");
            }

            if (resultInfo.RetrievalMode == ResultRetrievalMode.StoreRemotely)
            {
                throw new ArgumentException(string.Format("Invalid result type: {0}", resultInfo.RetrievalMode), "resultInfo");
            }

            byte[] content = IOHelper.ReadFile(localPath);

            // Send Result.
            proxy.SendResult(new ResultContent(resultInfo.Id, jobId, content));

            logger.Debug("Sent Result: {0}", resultInfo.FileName);

            return true;
        }
    }
}
