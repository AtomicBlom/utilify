using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Utilify.Platform.Executor
{
    internal class JobGetter : PlatformServiceBase
    {
        private IJobManager proxy;
        private JobTable jobTable;

        private EventWaitHandle availableJobEvent;
        private int pollInterval = 1000;

        private ExecutorConfiguration executorConfiguration;

        internal JobGetter(JobTable jobTable)
        {
            if (jobTable == null)
                throw new ArgumentNullException("jobTable");
            
            this.jobTable = jobTable;

            this.availableJobEvent = new AutoResetEvent(true);
        }


        // Comments about Abort Job functionality which has now been implemented as follows:
        // - Job Mappings are kept at the Manager until the Job is COMPLETE
        // - Executor asks the Manager which of the Jobs still mapped to it have been marked as CANCELLED/ABORTED
        // - Executor cancels these Jobs locally and discards them.
        //
        // Thought: Have a thread for each Job running through the Executor, with current Job* threads being Helper classes
        //          to perform certain actions sequentially. This makes it simple to abort/cancel jobs, just kill the thread.
        //          Otherwise how do we know where to kill the Job? How do we kill it instantly?
        //
        // Another Thought: Have to reduce the number of threads in the executor - since too many threads causes performance problems.
        //          So having 4 threads at present is already too much - when we could combine two or more of them.
        // 
        //          Threads are nice. Don't kill them unless there is a real reason for it.
        //
        protected override void Run()
        {
            CheckValidState();

            logger.Debug("Running in JobGetter...");

            // Tell the Manager which Jobs are still able to be run (in case some jobs can be recovered) before starting to request new jobs.
            if (!TryUpdateJobs(new string[0]))
            {
                logger.Error("Unable to update current Jobs.");
                OnError(new InternalServerException("Could not update current jobs"), true);
                return;
            }

            string executorId = this.executorConfiguration.Id;
            while (true)
            {

                try
                {
                    //logger.Debug("Waiting for jobs..");
                    availableJobEvent.WaitOne(pollInterval, false);
                    //logger.Debug("Waited long enough.. " + IsStopRequested);

                    if (IsStopRequested)
                        break;

                    //logger.Debug("Testing to see if we should get jobs...");
                    //logger.Debug(String.Format("jobs:{0} cpus:{1}", jobTable.Count, executorConfiguration.Processors.Length));
                    //if we have less # of jobs than twice the # of CPUs, we ask for more, or else we don't ask for jobs
                    //we should only be asking for a certain number of jobs.
                    if (jobTable.Count < (2 * executorConfiguration.Processors.Length))
                    {
                        // Get Jobs from the Manager.
                        JobSubmissionInfo[] jobs = proxy.GetJobs(executorId);
                        //logger.Debug("Got {0} Jobs from Manager to Process.", jobs.Length);
                        if (jobs != null && jobs.Length > 0)
                        {
                            logger.Info("Got {0} Jobs from Manager to Process.", jobs.Length);
                            ProcessJobs(jobs);
                        }
                    }
                    
                    // Get Cancelled Jobs from the Manager.
                    string[] jobsToCancel = proxy.GetCancelledJobs(executorId);
                    // Remove from JobTable and mark them as cancelled.
                    if (jobsToCancel != null && jobsToCancel.Length > 0)
                        jobTable.CancelJobs(new List<string>(jobsToCancel));

                }
                catch (Utilify.Platform.CommunicationException cx)
                {
                    //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                    logger.Warn("Communication with the Manager failed. Could not get Jobs ", cx);
                    Thread.Sleep(BackOff.GetNextInterval());
                }
                catch (InternalServerException ix) //insulate exctor from the Manager's exceptions
                {
                    logger.Warn("Server exception: ", ix);
                    Thread.Sleep(BackOff.GetNextInterval()); //back off for a bit
                }
                catch (Exception e)
                {
                    OnError(e, true);
                    break;
                }
            }
        }

        private bool TryUpdateJobs(String[] currentJobs)
        {
            bool updatedJobs = false;
            while (!updatedJobs)
            {
                try
                {
                    string executorId = this.executorConfiguration.Id;
                    logger.Info("Updating Current Jobs. {0} Jobs.", currentJobs.Length);
                    proxy.UpdateCurrentJobs(new Utilify.Platform.Contract.JobExecutionInfo(executorId, currentJobs));
                    updatedJobs = true;
                }
                catch (Utilify.Platform.CommunicationException cx)
                {
                    if (BackOff.NumberOfRetries > BackOff.MaxRetries)
                        break;

                    //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                    int backOffInterval = BackOff.GetNextInterval();
                    logger.Warn("Could not update jobs. Backing off for {1} before trying again...",
                        cx, Helper.GetFormattedTimeSpan(backOffInterval));

                    Thread.Sleep(backOffInterval);
                }
            }

            return updatedJobs;
        }

        /// <summary>
        /// Validates the submission info retrieved from the manager,
        /// and creates job objects and puts them into the internal jobTable.
        /// </summary>
        /// <param name="jobs"></param>
        private void ProcessJobs(JobSubmissionInfo[] jobs)
        {
            foreach (JobSubmissionInfo jobInfo in jobs)
            {
                // Create Job from the JobInfo
                if (jobInfo != null && !String.IsNullOrEmpty(jobInfo.JobId))
                {
                    // Issue 059: What to do if the Job received here has some problems (need to validate). Want to still create a Job, 
                    //       then set its Error field and set the status to Stopped. The Returner will then return the job with 
                    //       the error message to the Manager. If its missing basic info such as Id, then throw it away?
                    Job job = null;
                    bool isValid = ValidateJob(jobInfo, out job);
                    if (isValid)
                        jobTable.Add(job);
                    else
                        jobTable.AddInvalid(job);

                    job.ExecutionLog.AppendLine("Job submission info successfully downloaded from Manager.");
                    job.ExecutionLog.AppendLine(String.Format("Id:{0}, Platform:{1}, Type:{2}", job.JobId, job.ClientPlatform, job.JobType));
                }
                else
                {
                    // Cannot do anything with this JobInfo. Just log the problem and continue.
                    if (jobInfo == null)
                        logger.Error("Received a Null Job from the Manager. Discarding Job.");
                    else
                        logger.Error("Received a Job with Null or Empty Id from the Manager. Discarding Job.");
                }
            }
        }

        private void CheckValidState()
        {
            if (this.executorConfiguration != null)
                return;

            //get executor info from the internal state
            InternalState.WaitHandle.WaitOne();

            //if we still don't have a valid config, blow up:
            this.executorConfiguration = InternalState.ExecutorConfiguration;
            if (this.executorConfiguration == null)
                throw new InvalidOperationException("Executor configuration could not be retrieved");

            //now get the proxy 
            this.proxy = InternalState.JobManager;
        }

        private bool ValidateJob(JobSubmissionInfo jobInfo, out Job job)
        {
            StringBuilder log = new StringBuilder();
            StringBuilder error = new StringBuilder();
            bool isValid = true;

            logger.Debug("Validating Job retrieved from Manager. JobId: " + jobInfo.JobId);

            log.AppendLine("Validating Job retrieved from Manager. JobId: " + jobInfo.JobId);

            // Check JobId
            //if (String.IsNullOrEmpty(jobInfo.JobId))
            //    error.AppendLine("JobId is Null or Empty.");

            // Check ApplicationId
            if (String.IsNullOrEmpty(jobInfo.ApplicationId))
                error.AppendLine("ApplicationId is Null or Empty.");

            // Check JobInstance 
            if (jobInfo.GetJobInstance().Length == 0)
                error.AppendLine("JobInstance is Null or Empty.");

            // Check JobType
            // todoLater: Have some way to specify (configurability) which Job Types can be supported on this Executor and reject any Jobs
            //       that are not of the supported type.
            if (jobInfo.JobType != JobType.CodeModule &&
                jobInfo.JobType != JobType.NativeModule)
                error.AppendLine(String.Format("JobType {0} not supported by this Executor.", jobInfo.JobType));
            
            // Check ClientPlatform
            if (jobInfo.ClientPlatform != ClientPlatform.DotNet &&
                jobInfo.ClientPlatform != ClientPlatform.Java)
                error.AppendLine(String.Format("ClientPlatform {0} not supported by this Executor.", jobInfo.ClientPlatform));

            // Validate Dependencies
            foreach (DependencyInfo depInfo in jobInfo.GetDependencies())
            {
                if (String.IsNullOrEmpty(depInfo.Filename))
                    error.AppendLine("Dependency Filename is Null or Empty.");

                if (String.IsNullOrEmpty(depInfo.Hash))
                    error.AppendLine("Dependency Hash is Null or Empty.");

                if (String.IsNullOrEmpty(depInfo.Name))
                    error.AppendLine("Dependency Name is Null or Empty.");

                //if (depInfo.Type != DependencyType.ClrModule &&
                //    depInfo.Type != DependencyType.DataFile &&
                //    depInfo.Type != DependencyType.JavaModule &&
                //    depInfo.Type != DependencyType.NativeModule)
                //    error.AppendLine(String.Format("Dependency of Type {0} is not supported by this Executor.", depInfo.Type));
            }

            // Validate Results
            foreach (ResultInfo resInfo in jobInfo.GetExpectedResults())
            {
                // Job *could* still continue even though the ResultInfos declared are screwed. What do we do? Decide later.
                if (String.IsNullOrEmpty(resInfo.FileName))
                    error.AppendLine("Result Filename is Null or Empty.");
            }           
            
            // Now create the Job (if there were any Errors then the Job is Invalid)
            job = new Job(jobInfo);
            job.ExecutionError.AppendLine(error.ToString());
            job.ExecutionLog.AppendLine(log.ToString());

            if (error.Length > 0)
                isValid = false;

            if (!isValid)
                logger.Debug("Invalid job : \n" + error.ToString());

            return isValid;
        }

        // Issue 060: This event handler is to be called with the non-polling usage of the Executor.
        //       A component/service listening for notifications of mapped jobs from the manager, which will raise
        //       an event that this listens for, telling it to pull jobs. 
        //
        //       Also, in this mode the pollInterval for this component should be set to wait forever. 
        //       System.Threading.Timeout.Infinite (-1)
        internal void OnJobAvailable(object sender, EventArgs e)
        {
            availableJobEvent.Set();
        }

        protected internal override string Name
        {
            get { return "JobGetter"; }
        }
    }
}
