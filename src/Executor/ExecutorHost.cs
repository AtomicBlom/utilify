using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Utilify.Platform.Configuration;

namespace Utilify.Platform.Executor
{
    public sealed class ExecutorHost : IDisposable
    {
        private List<PlatformServiceBase> services;
        private Logger logger;
        
        private IResourceMonitor rmProxy;

        public event EventHandler<EventArgs> OnShutDown;

        public ExecutorHost() : this(ConfigurationHelper.GetConnectionSettings(ServiceType.ResourceManager, 
            System.Configuration.ConfigurationUserLevel.None, true)) 
        {
        }

        public ExecutorHost(ConnectionSettings settings)
        {
            //listen to unhandled exceptions in this appdomain
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            logger = new Logger();

            PrintStartInfo();

            services = new List<PlatformServiceBase>();

            //string configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            //if (configFile != null)
            //{
            //    RemotingConfiguration.Configure(configFile, false);  //let security be enabled in the config
            //}

            //todoPerf: look to combine these threads to make it 3 (or less) from the 5 we have now: perhaps we could better use Auto/ManualResetEvents here 
            //could combine: jobGetter + Preparer, and jobExecutor + Returner.

            // 1) Firewalled :
            // Polls the manager to find out if a job is ready to execute. Once a job is found, it will then 
            // ask the manager for the job, queue the job and raise an event to inform local listeners that a job has arrived.
            // 
            // 2) Non-Firewalled : (not implemented at the moment)
            // Listens for messages from the manager to find out if a job is ready to execute. Once a job is found, will then 
            // ask th

            rmProxy = ProxyFactory.CreateProxyInstance<IResourceMonitor>(settings);

            // Create JobTable to coordinate Job execution.
            JobTable jobTable = new JobTable();

            //1. Create all services
            ResourceManager resourceManager = new ResourceManager(settings);
            services.Add(resourceManager);

            // Give the jobTable here to all the guys who need to share it.
            JobGetter jobGetter = new JobGetter(jobTable);
            services.Add(jobGetter);

            JobPreparer jobPreparer = new JobPreparer(jobTable);
            services.Add(jobPreparer);

            JobExecutor jobExecutor = new JobExecutor(jobTable);
            services.Add(jobExecutor);

            JobReturner jobReturner = new JobReturner(jobTable);
            services.Add(jobReturner);

            //2. Subscribe to job status events
            jobTable.ExecutionStatusChanged += jobPreparer.OnExecutionStatusChanged;
            jobTable.ExecutionStatusChanged += jobExecutor.OnExecutionStatusChanged;
            jobTable.ExecutionStatusChanged += jobReturner.OnExecutionStatusChanged;

            //3. Subscribe to service error events
            foreach (PlatformServiceBase service in services)
            {
                service.Error += new EventHandler<ServiceErrorEventArgs>(InternalService_Error);
            }

            //4. Reset the internal state so that the threads will wait
            InternalState.WaitHandle.Reset();
        }

        private void PrintStartInfo()
        {
            try
            {
                StringBuilder sb = new StringBuilder(Environment.NewLine);
                sb.AppendLine(Helper.GetVersionString(this.GetType().Assembly));
                sb.AppendLine(Helper.GetEnvironment()).AppendLine();
                logger.Info(sb.ToString());
            }
            catch (Exception ex)
            {
                logger.Warn("Error getting environment information...", ex);
            }
        }

        public void StartService()
        {
            VerifyDisposed();

            //first check if the server cert we have is ok
            //CheckServerCertificate();

            //start all the services.
            foreach (PlatformServiceBase service in services)
            {
                logger.Debug("Starting service {0}", service.Name);
                service.Start(); 
            }
        }

        public void StopService()
        {
            // Stop all threads/services
            StopService(ServiceStopOptions.Default);
        }

        /// <summary>
        /// Stops all the services (and tries to inform the manager that this executor is stopping).
        /// </summary>
        /// <param name="options"></param>
        public void StopService(ServiceStopOptions options)
        {
            TryInformManager();

            // Stop all threads/services
            foreach (PlatformServiceBase service in services)
            {
                service.Stop(options);
            }
        }

        #region Error handling

        void InternalService_Error(object sender, ServiceErrorEventArgs e)
        {
            string senderName = (sender == null) ? "Unknown" : sender.GetType().Name;
            logger.Error(string.Format("Error in service : {0}. IsTerminating = {1}", senderName, e.IsTerminating), e.Error);

            //One way to deal with this would be to restart the service.
            //But since we don't know if it will still work, let's just bring the whole thing down!
            if (e.IsTerminating)
            {
                StopService();
                EventHelper.RaiseEvent<EventArgs>(OnShutDown, this, EventArgs.Empty);
            }
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            TryInformManager();
            EventHelper.RaiseEvent<EventArgs>(OnShutDown, this, EventArgs.Empty);

            logger.Error("Unhandled exception in current domain - {0}. IsTerminating = {1}, Exception = {2}",
                AppDomain.CurrentDomain.FriendlyName, e.IsTerminating, e.ExceptionObject.ToString());   
        }

        private bool TryInformManager()
        {
            bool informed = false;
            IJobManager jmProxy = InternalState.JobManager;
            if (jmProxy != null)
            {
                try
                {
                    //it can be null if the exctor could never register!
                    if (InternalState.ExecutorConfiguration != null)
                        jmProxy.UpdateCurrentJobs(new Utilify.Platform.Contract.JobExecutionInfo(InternalState.ExecutorConfiguration.Id, new String[0]));

                    informed = true;
                }
                catch (ObjectDisposedException) { }
                catch (Exception ex)
                {
                    logger.Warn("Could not update jobs to manager: " + ex.Message);
                } //ignore if we can't do this
            }
            return informed;
        }

        #endregion

        #region IDisposable Members
        private bool disposed = false;

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                //clean up managed stuff
                IDisposable disposable = rmProxy as IDisposable;
                if (disposable != null)
                    disposable.Dispose();

                disposable = InternalState.JobManager as IDisposable;
                if (disposable != null)
                    disposable.Dispose();

                disposable = null;
#if MONO
                //close down any remoting connections
                RemotingHelper.ShutdownChannels();
#endif
            }

            //we have no base class to call dispose on, so nothing more to do here.

            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
        #endregion
    }
}
