using System;
using System.IO;
using System.Threading;

namespace Utilify.Platform.Executor
{
    internal class JobExecutor : PlatformServiceBase
    {
        private JobTable jobTable;
        private EventWaitHandle readyJobEvent;
        private JobDispatcher dispatcher;
        private int pollInterval = 100;
        private int defaultPort = 50210;
        private int port;
        private int maxNumberOfExecutorsOnOneMachine = 100; // This will limit us to only trying 100 ports
        private ExecutorConfiguration executorConfiguration;

        private const int MaxFailureCount = 2;

        internal JobExecutor(JobTable jobTable)
        {
            if (jobTable == null)
                throw new ArgumentNullException("jobTable");

            bool connected = false;
            int currentRetry = 0;
            while (!connected)
            {
                try
                {
                    //todoConfig : get this listening port from from the config. 
                    //             does it really need to be configurable? why? if so, make it an advanced/optional configuration most users
                    //             will never need to know it even exists.
                    dispatcher = new JobDispatcher( defaultPort + currentRetry ); 
                    port = defaultPort + currentRetry;
                    connected = true;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    if (IsStopRequested)
                        break;

                    // Don't bother sleeping here. We know we can find a port soon if we just keep trying.
                    logger.Warn("Could not start internal Executor server on port {0}. Retrying at next port...", defaultPort + currentRetry);

                    currentRetry++;
                    if (currentRetry > maxNumberOfExecutorsOnOneMachine)
                    {
                        int backOffInterval = BackOff.GetNextInterval();
                        logger.Info("All ports in range are busy, cannot start server. Backing off for {0} and will retry soon.",
                            Helper.GetFormattedTimeSpan(backOffInterval));
                        Thread.Sleep(backOffInterval);
                    }
                }
            }

            this.jobTable = jobTable;
            this.readyJobEvent = new AutoResetEvent(false);

            logger.Debug("JobExecutor successfully started on port " + port);
        }

        protected internal override string Name
        {
            get { return "JobExecutor"; }
        }

        protected override void Run()
        {
            CheckValidState();

            while (true)
            {
                try
                {
                    readyJobEvent.WaitOne(pollInterval, false);

                    if (IsStopRequested)
                        break;

                    //if there are any running jobs, only then ask the dispatcher if any have finished.
                    //because we just slept it is best to check for finished jobs first.
                    int runningJobs = jobTable.GetCount(ExecutionStatus.Running);
                    if (runningJobs > 0)
                    {
                        logger.Debug("Got {0} running jobs to check for...", runningJobs);

                        foreach (var value in Enum.GetValues(typeof(ClientPlatform)))
                        {
                            //need to get finished jobs seperately for each platform because:
                            //we need to communicate with a client process for each platform
                            //and deal with communication issues seperately.

                            var platform = (ClientPlatform)value;

                            //get completed jobs for types of all platforms with running jobs.
                            //(each platform has its own process)
                            if (jobTable.HasRunningJobs(platform))
                            {
                                TryGetCompletedJobs(platform);
                            }
                        }
                    }

                    //we only want one job to be running on a single processor at any time, so check that.
                    runningJobs = jobTable.GetCount(ExecutionStatus.Running); // check again
                    if (runningJobs < executorConfiguration.Processors.Length)
                    {
                        // Get all Ready Jobs for execution.
                        Job[] jobs = jobTable.GetJobs(ExecutionStatus.Ready);
                        if (jobs.Length > 0)
                        {
                            logger.Info("Got {0} Jobs to Execute.", jobs.Length);
                            DispatchJobs(jobs);
                        }
                    }
                }
                catch (Exception e)
                {
                    OnError(e, true);
                    break;
                }
            }

            try
            {
                //important to try and shutdown the dispatcher, which will signal out-of-process real executors to die
                IDisposable disp = dispatcher as IDisposable;
                if (disp != null)
                    disp.Dispose();
            }
            catch { } //don't worry if this fails.
        }

        private void CheckValidState()
        {
            if (this.executorConfiguration != null)
                return;

            //get executor info from the internal state
            InternalState.WaitHandle.WaitOne();

            //if we still don't have a valid config, blow up:
            this.executorConfiguration = InternalState.ExecutorConfiguration;
            if (this.executorConfiguration == null)
                throw new InvalidOperationException("Executor configuration could not be retrieved");
        }

        /// <summary>
        /// Retrieves the completd jobs from the JobDispatcher (which gets it from the client process)
        /// and updates the jobTable.
        /// </summary>
        private void TryGetCompletedJobs(ClientPlatform platform)
        {
            JobCompletionInfo[] finishedJobs = null;

            try
            {
                //logger.Debug("Trying to get completed job info from dispatcher...");
                finishedJobs = dispatcher.GetCompletedJobs(platform);

                //if there are any finished jobs, process them
                if (finishedJobs != null && finishedJobs.Length > 0)
                {
                    logger.Info("Got {0} Jobs that have finished executing", finishedJobs.Length);

                    foreach (JobCompletionInfo jobInfo in finishedJobs)
                    {
                        // Handle completed Job. Any Execution errors should be forwarded from the ExecutionManager.
                        Job completedJob = jobTable.GetJob(jobInfo.JobId, ExecutionStatus.Running);

                        // Check if we should continue
                        // If the job isn't retrieved from the JobTable (having been aborted) or if it's been set
                        // to cancelled then we skip this job and continue with the next job.
                        if (completedJob == null || completedJob.IsCancelled)
                            continue;

                        // Assign the finalised Job instance.
                        completedJob.FinalJobInstance = jobInfo.GetJobInstance();
                        completedJob.ExecutionExceptionInstance = jobInfo.GetJobException();

                        // Append any execution logs or errors.
                        completedJob.ExecutionError.AppendLine(jobInfo.ExecutionError);
                        completedJob.ExecutionLog.AppendLine(jobInfo.ExecutionLog);
                        completedJob.CpuTime = jobInfo.CpuTime;
                        // Inform others that the Job has finished executing.
                        completedJob.ExecutionLog.AppendLine("Job execution completed.");
                        jobTable.ChangeStatus(completedJob, ExecutionStatus.Stopped);
                        logger.Debug(String.Format("Updating Job {0} status to 'Stopped'.", completedJob.JobId));
                    }
                }
                else
                {
                    logger.Debug("Got 0 finished jobs from dispatcher.");
                }
            }
            catch (IOException iox)
            {
                //failed to communicate with the dispatcher client processes.
                //try again in the next loop.
                logger.Warn("Failed to communicate with the dispatcher client process. Will try again in next round..." + iox.Message);
            }
            catch (PlatformException px)
            {
                //looks like we lost the client process: we may need to reset the jobs and submit again.
                var runningJobs = jobTable.GetJobs(ExecutionStatus.Running);
                foreach (var job in runningJobs)
                {
                    if (job.ClientPlatform == platform)
                    {
                        job.FailureCount += 1;
                        if (job.FailureCount >= MaxFailureCount)
                        {
                            logger.Debug("Resetting job {0} to Stopped on Executor", job.JobId);
                            job.ExecutionError.AppendLine("Could not communicate with Execution process on Executor: " + px.Message);
                            jobTable.ChangeStatus(job, ExecutionStatus.Stopped); //reset it
                        }
                        else
                        {
                            logger.Debug("Resetting job {0} to Ready on Executor", job.JobId);
                            jobTable.ChangeStatus(job, ExecutionStatus.Ready); //reset it
                        }
                    }
                }
                logger.Warn(px.Message);
            }
        }

        /// <summary>
        /// Sends the jobs to the client process using the JobDispatcher.
        /// These jobs are executed in the client process.
        /// </summary>
        /// <param name="jobs"></param>
        private void DispatchJobs(Job[] jobs)
        {
            foreach (Job job in jobs)
            {
                try
                {
                    logger.Debug("Job approaching target execution engine. " + job.JobId);
                    job.ExecutionLog.AppendLine("Job approaching target execution engine.");

                    // Check if we should continue
                    if (job.IsCancelled)
                        continue; // Skip this Job. We thought it was Ready but it became Cancelled while we were waiting.

                    // Issue 058: Separate process execution.
                    dispatcher.ExecuteJob(job.JobId,
                                job.ApplicationId,
                                job.JobDirectory,
                                job.ApplicationDirectory,
                                job.JobType,
                                job.ClientPlatform,
                                job.InitialJobInstance);

                    // Issue 058: Implement Java Executor.
                    logger.Debug("Job started execution. " + job.JobId);
                    job.ExecutionLog.AppendLine("Job started execution.");
                    jobTable.ChangeStatus(job, ExecutionStatus.Running);
                }
                catch (IOException e)
                {
                    logger.Warn("Job failed to begin executing.", e);
                    job.ExecutionError.AppendLine("Job failed to begin executing.");
                    jobTable.ChangeStatus(job, ExecutionStatus.Stopped);
                }
            }
        }

        internal void OnExecutionStatusChanged(object sender, ExecutionStatusEventArgs e)
        {
            switch (e.Status)
            {
                case ExecutionStatus.Ready:
                    readyJobEvent.Set();
                    break;
                case ExecutionStatus.Cancelled:
                    // Cancel Job via the dispatcher.
                    try
                    {
                        logger.Debug("Aborting Job:" + e.Job.JobId + ".. triggered from Execution Status Changed.");
                        dispatcher.AbortJob(e.Job);
                    }
                    catch (IOException iox) 
                    {
                        logger.Warn("Could not abort job: " + iox.Message);
                    }

                    break;
            }
        }
    }
}
