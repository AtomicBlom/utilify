using System;
using System.Collections.Generic;
using System.Text;


namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Result : 
        ITransformable<Utilify.Platform.ResultInfo>,
        ITransformable<Utilify.Platform.ResultStatusInfo>
    {
        private Guid id;
        private BinaryData content;
        private Guid? jobId;
        private string fileName;
        private ResultRetrievalMode retrievalMode;
        private long version = DataStore.UnsavedVersionValue;

        protected Result() { }

        /// <summary>
        /// Creates an instance of a new Result object with the specified result info.
        /// </summary>
        /// <param name="info"></param>
        public Result(Utilify.Platform.ResultInfo info) : this()
        {
            if (info == null)
                throw new ArgumentNullException("info");

            this.FileName = info.FileName;
            this.RetrievalMode = info.RetrievalMode;
            this.content = null;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        public virtual Guid Id
        {
            get { return id; }
            protected set
            {
                if (value == Guid.Empty)
                    throw new ArgumentException(Messages.ArgumentEmpty, "id");
                id = value;
            }
        }

        public virtual Guid? JobId
        {
            get { return jobId; }
            protected set 
            {
                //looks like the ORM sets this value multiple times - and sometimes the value is empty initially before being updated.
                //if (value == Guid.Empty)
                //    throw new ArgumentException("Job id cannot be empty", "jobId");
                jobId = value;
            }
        }

        public virtual string FileName
        {
            get { return fileName; }
            protected set 
            {
                if (value == null)
                    throw new ArgumentNullException("fileName");
                if (value.Trim().Length == 0)
                    throw new ArgumentException(Messages.ArgumentEmpty, "fileName"); 
                fileName = value;
            }
        }

        public virtual ResultRetrievalMode RetrievalMode
        {
            get { return retrievalMode; }
            protected set { retrievalMode = value; }
        }

        public virtual byte[] GetContent()
        {
            if (content == null)
                return null;
            else
                return content.GetValue();
        }

        protected internal virtual void SetContent(byte[] content)
        {
            if (content == null)
                throw new ArgumentNullException("content");
            //if (content.Length == 0)
            //    throw new ArgumentException("Content cannot be empty", "content");

            this.content = new BinaryData(content);
        }

        public virtual bool HasContent
        {
            get
            {
                return (this.content != null);
            }
        }

        public virtual bool IsResolved
        {
            get
            {
                bool isResolved = false;
                isResolved = (this.RetrievalMode == ResultRetrievalMode.StoreRemotely) ||
                    (content != null);

                return isResolved;
            }
        }

        public static List<Result> ConvertFromInfo(IEnumerable<Utilify.Platform.ResultInfo> infos)
        {
            if (infos == null)
                throw new ArgumentNullException("infos");

            List<Result> list = new List<Result>();
            foreach (Utilify.Platform.ResultInfo result in infos)
            {
                if (result == null)
                    throw new ArgumentException("Elements of the result info list cannot be null");

                list.Add(new Result(result));
            }

            return list;
        }

        #region ITransformable<ResultInfo> Members

        Utilify.Platform.ResultInfo ITransformable<Utilify.Platform.ResultInfo>.Transform()
        {
            return new Utilify.Platform.ResultInfo(this.Id.ToString(), this.FileName, this.RetrievalMode);
        }

        #endregion

        #region ITransformable<ResultStatusInfo> Members

        Utilify.Platform.ResultStatusInfo ITransformable<Utilify.Platform.ResultStatusInfo>.Transform()
        {
            Utilify.Platform.ResultStatusInfo info =
                new Utilify.Platform.ResultStatusInfo(this.Id.ToString(), 
                    this.JobId.GetValueOrDefault().ToString(),
                    new Utilify.Platform.ResultInfo(this.FileName, this.RetrievalMode));
            return info;
        }

        #endregion
    }
}
