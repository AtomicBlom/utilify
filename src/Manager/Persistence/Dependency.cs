using System;
using System.Collections.Generic;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Dependency : ITransformable<DependencyInfo> //todoDoc
    {
        private Guid id; //persistence Id : assigned at the server
        private DateTime cached;
        private string fileName;
        private string hash;
        private DateTime modified;
        private string name;
        private long size;
        private DependencyType type;
        private BinaryData content;
        private long version = DataStore.UnsavedVersionValue;

        protected Dependency() {}

        public Dependency(DependencyInfo info) : this()
        {
            if (info == null)
                throw new ArgumentNullException("info");

            this.Name = info.Name;
            this.Type = info.Type;
            this.FileName = info.Filename;
            this.Modified = info.Modified;
            this.Size = info.Size;
            this.Hash = info.Hash;

            this.content = null;
            this.cached = Helper.MinUtcDate;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        public virtual Guid Id
        {
            get { return id; }
            protected set { id = value; }
        }

        public virtual string FileName
        {
            get { return fileName; }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException("fileName");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Dependency file/url cannot be empty", "fileName");
                fileName = value;
            }
        }

        public virtual string Hash  // hashes can be null - since url deps don't have hashes
        {
            get { return hash; }
            protected set { hash = value; }
        }

        public virtual DateTime Modified
        {
            get { return modified; }
            protected set { modified = Helper.MakeUtc(value); }
        }

        public virtual DateTime Cached
        {
            get { return cached; }
            protected set { cached = Helper.MakeUtc(value); }
        }

        public virtual string Name
        {
            get { return name; }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Dependency name cannot be empty", "name");

                name = value;
            }
        }

        public virtual long Size
        {
            get { return size; }
            protected set { size = value; }
        }

        public virtual bool IsResolved
        {
            get 
            {
                bool isResolved = false;
                //for remote urls, isResolved should always return true
                //otherwise, we should have some valid binary data content with length > 0
                isResolved = (this.Type == DependencyType.RemoteUrl) ||
                    (content != null);

                return isResolved; 
            }
        }

        public virtual DependencyType Type
        {
            get { return type; }
            protected set
            {
                type = value;
            }
        }

        #region DateTime persistence helpers (using ticks)

        protected virtual long ModifiedTicks
        {
            get { return modified.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("modified", string.Format("Modified time cannot be less than {0}", DateTime.MinValue));
                modified = new DateTime(value, DateTimeKind.Utc);
            }
        }

        protected virtual long CachedTicks
        {
            get { return cached.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("cached", string.Format("Cached time cannot be less than {0}", DateTime.MinValue));
                cached = new DateTime(value, DateTimeKind.Utc);
            }
        }

        #endregion

        protected internal virtual long ContentId
        {
            get
            {
                return (this.content != null) ? this.content.Id : 0;
            }
        }

        public virtual byte[] GetContent()
        {
            if (content == null)
                return null;
            else
                return content.GetValue();
        }

        public virtual void SetContent(byte[] content)
        {
            if (content == null)
                throw new ArgumentNullException("content");
            if (content.Length == 0)
                throw new ArgumentException("Content cannot be empty", "content");
 
            this.content = new BinaryData(content);
            this.cached = DateTime.UtcNow;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            bool areEqual = false;
            Dependency other = obj as Dependency;
            areEqual = (other.Name == this.Name) &&
                (other.Type == this.Type) &&
                (other.FileName == this.FileName) &&
                (other.Modified == this.Modified) &&
                (other.Size == this.Size) && 
                (other.Hash == this.Hash);
            return areEqual;
        }

        private int hashCode = 0;
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 42 ^ modified.GetHashCode() ^ cached.GetHashCode();
                if (type != 0)
                    hashCode = type.GetHashCode() ^ hashCode;
                if (hash != null)
                    hashCode = hash.GetHashCode() ^ hashCode;
                if (fileName != null)
                    hashCode = fileName.GetHashCode() ^ hashCode;
                if (name != null)
                    hashCode = name.GetHashCode() ^ hashCode;
                if (size != 0)
                    hashCode = hashCode ^ ((int)size ^ (int)(size >> 32));
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        public override string ToString()
        {
            return string.Format("Dependency [{3}]: {0}, {1}, type: {2}", name, fileName, type, id);
        }

        public static List<Dependency> ConvertFromInfo(IEnumerable<DependencyInfo> infos)
        {
            if (infos == null)
                throw new ArgumentNullException("infos");

            List<Dependency> list = new List<Dependency>();
            foreach (DependencyInfo dep in infos)
            {
                if (dep == null)
                    throw new ArgumentException("Elements of the dependency info list cannot be null");

                list.Add(new Dependency(dep));
            }
            
            return list;
        }

        #region ITransformable<DependencyInfo> Members

        DependencyInfo ITransformable<DependencyInfo>.Transform()
        {
            return new DependencyInfo(this.Id.ToString(), this.Name, this.FileName,
                this.Modified, this.Size, this.Type, this.Hash);
        }

        #endregion
    }
}