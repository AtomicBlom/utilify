using System;
using System.Collections.Generic;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    //TODO: Upgrade to latest .NET Fx and other libs for cloud version?
    //explore opn core / crust model of licensing
    [Serializable]
    public class Application : ITransformable<ApplicationSubmissionInfo>
    {
        private Guid id = Guid.Empty; //persistence Id : assigned at server
        private string name;
        private string clientHost;
        private IList<Dependency> dependencies;
        private Qos qos;
        private string username;
        private DateTime creationTime;
        private ApplicationStatus status = ApplicationStatus.UnInitialized;
        private long version = DataStore.UnsavedVersionValue;
        private JobStatistics jobStatistics; //assigned by NHibernate
        private DateTime mostRecentJobStartTime;

        //needed for NHibernate proxy
        protected Application() { }
        
        //Issue 021: enable app security : how to pass credentials. etc. will effect these ctors.

        /// <summary>
        /// Creates an instance of the Application from the submission info object
        /// </summary>
        /// <param name="appInfo"></param>
        public Application(ApplicationSubmissionInfo appInfo, string username) : this()
        {
            if (appInfo == null)
                throw new ArgumentNullException("appInfo");
            if (String.IsNullOrEmpty(username))
                throw new ArgumentException("username cannot be null or empty", "username");

            this.Name = appInfo.ApplicationName;
            this.Username = username;
            this.ClientHost = appInfo.ClientHost;
            this.Qos = new Qos(appInfo.Qos);
            this.CreationTime = DateTime.UtcNow;
            this.dependencies = Dependency.ConvertFromInfo(appInfo.GetDependencies()).ToArray();
            this.MostRecentJobStartTime = DateTime.MaxValue;

            this.Status = ApplicationStatus.Submitted; //default state after the ctor, for an Application on the Manager.
        }

        /// <summary>
        /// Gets the name of the application submitted
        /// </summary>
        public virtual string Name
        {
            get { return name; }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException("name", "Application name cannot be null");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application name cannot be empty");

                name = value;
            }
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        /// <summary>
        /// Gets the id of the application
        /// </summary>
        public virtual Guid Id
        {
            get { return id; }
            protected set { id = value; }
        }
        /// <summary>
        /// Gets the host name of the client that submitted the application
        /// </summary>
        public virtual string ClientHost
        {
            get { return clientHost; }
            protected set 
            {
                if (value == null)
                    throw new ArgumentNullException("clientHost", "Client hostname cannot be null");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Client hostname cannot be empty");
                
                clientHost = value; 
            }
        }

        //Currently not used anywhere
        // <summary>
        // Gets the job statistics for the application
        // </summary>
        //This is NOT mapped via the ORM (to save the # of queries): any call that needs this should explicitly populate it.
        //internal virtual JobStatistics JobStatistics
        //{
        //    get { return jobStatistics; }
        //    private set { jobStatistics = value; }
        //}

        /// <summary>
        /// Gets the dependencies of the application
        /// </summary>
        public virtual Dependency[] GetDependencies()
        {
            return Helper.GetCopy<Dependency>(dependencies);
        }

        //This is needed to be able to update the dependency links with existing cached dependencies in the db
        protected internal virtual IList<Dependency> Dependencies
        {
            get
            {
                return this.dependencies;
            }
        }

        /// <summary>
        /// Gets the QoS requirements for the  application
        /// </summary>
        public virtual Qos Qos
        {
            get { return qos; }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException("qos", "Qos cannot be null");

                qos = value;
            }
        }

        /// <summary>
        /// Gets the user that submitted the application
        /// </summary>
        public virtual string Username
        {
            get { return username; }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException("username", "Application username cannot be null");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application username cannot be empty");

                username = value;
            }
        }

        /// <summary>
        /// Gets the date/time when the application was created/submitted
        /// </summary>
        public virtual DateTime CreationTime
        {
            get { return creationTime; }
            protected set { creationTime = Helper.MakeUtc(value); }
        }

        //This is used for scheduling and probably should be inside the JobStatistics. Prevents the same app being scheduled everytime
        public virtual DateTime MostRecentJobStartTime
        {
            get { return mostRecentJobStartTime; }
            protected set
            {
                mostRecentJobStartTime = Helper.MakeUtc(value); 
            }
        }

        #region DateTime persistence helpers (using ticks)

        //used by the persistence layer
        protected virtual long CreationTimeTicks
        {
            get { return creationTime.Ticks; }
            set 
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("creationTime", string.Format("Creation time cannot be less than {0}", DateTime.MinValue));
                creationTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        //used by the persistence layer
        protected virtual long MostRecentJobStartTimeTicks
        {
            get { return mostRecentJobStartTime.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("mostRecentJobStartTime", string.Format("Most recent job start time cannot be less than {0}", DateTime.MinValue));
                mostRecentJobStartTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        #endregion

        /// <summary>
        /// Gets the status of the application
        /// </summary>
        /// <exception cref="Utilify.Platform.Manager.InvalidTransitionException">
        /// If the status is set to an invalid value, which is not allowed for the current status.
        /// </exception>
        public virtual ApplicationStatus Status
        {
            get { return status; }
            set 
            {
                //IMP: We double-check here as well as the caller to ensure correct state transitions are happening.
                string msg = "Invalid argument 'status'. Application state cannot transition from {0} to {1}.";
                //only certain state transitions are allowed:
                //Un-initialized / Paused -> Submitted
                //Submitted / Paused -> Ready
                //Submitted / Ready  -> Paused
                //Submitted / Ready / Paused -> Stopped 
                switch (value)
                {
                    case ApplicationStatus.Submitted:
                        if (status != ApplicationStatus.UnInitialized &&
                            status != ApplicationStatus.Paused)
                            throw new InvalidTransitionException(string.Format(msg, status, value));
                        break;
                    case ApplicationStatus.Ready:
                        if (status != ApplicationStatus.Submitted &&
                            status != ApplicationStatus.Paused)
                            throw new InvalidTransitionException(string.Format(msg, status, value));
                        break;
                    case ApplicationStatus.Paused:
                        if (status != ApplicationStatus.Submitted &&
                            status != ApplicationStatus.Ready)
                            throw new InvalidTransitionException(string.Format(msg, status, value));
                        break;
                    case ApplicationStatus.Stopped:
                        if (status != ApplicationStatus.Submitted &&
                            status != ApplicationStatus.Ready &&
                            status != ApplicationStatus.Paused)
                            throw new InvalidTransitionException(string.Format(msg, status, value));
                        break;
                    default:
                        throw new InvalidTransitionException(string.Format(msg, status, value));
                }
                status = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;

            Application other = obj as Application;
            bool equals = 
                (other.ClientHost == this.ClientHost) &&
                (other.Name == this.Name) &&
                (other.Qos.Equals(this.Qos))  &&
                (Helper.AreEqual(other.CreationTime, this.CreationTime)) &&
                (other.Status == this.Status) && 
                (other.Username == this.Username);

            return equals;
        }

        private int hashCode;
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 42 ^ creationTime.GetHashCode();
                if (name != null)
                    hashCode = name.GetHashCode() ^ hashCode;
                if (clientHost != null)
                    hashCode = clientHost.GetHashCode() ^ hashCode;
                if (qos != null)
                    hashCode = qos.GetHashCode() ^ hashCode;
                if (status != 0)
                    hashCode = status.GetHashCode() ^ hashCode;
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        public override string ToString()
        {
            return string.Format("{0}[{1}] : CreationTime : {2:r}, Client : {3}, User: {4}, Status : {5}",
                this.GetType().FullName, this.Name, this.CreationTime, this.ClientHost, this.Username, this.Status);
        }

        #region ITransformable<ApplicationSubmissionInfo> Members

        ApplicationSubmissionInfo ITransformable<ApplicationSubmissionInfo>.Transform()
        {
            List<DependencyInfo> depInfos =
                DataStore.Transform<Dependency, DependencyInfo>(dependencies);
            return new ApplicationSubmissionInfo(this.Id.ToString(), this.Name, depInfos, this.Qos.GetInfo());
        }

        #endregion
    }
}