using System;
using System.Runtime.Serialization;

using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Processor : 
        IUpdatable<ProcessorSystemInfo>, ITransformable<ProcessorPerformanceInfo>, ITransformable<ProcessorLimitInfo>
    {
        private long id; //persistence identifier
        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        private Guid? executorId; // link to system identifier

        private string name; // OS's name/id for this CPU used for linking by performance and limit infos
        private string description; // CPU string
        private string vendor;
        private Architecture architecture;
        private double speedMax; //(in  Ghz)
        private double speedCurrent; //(in Ghz)
        //private int powerLevel;
        private double loadTotalCurrent; //(in %)
        private double loadUsageCurrent; //(in %)
        private double loadUsageLimit; //(in %)
        private long version = DataStore.UnsavedVersionValue;

        //hmm...can we somehow make this protected?
        //this is needed since we use generics to update a collection of processors
        public Processor(){}

        public Processor(ProcessorSystemInfo info) : this()
        {
            LoadFrom(info); 
        }

        private void LoadFrom(ProcessorSystemInfo info)
        {
            this.Name = info.Name;
            this.Description = info.Description;
            this.Vendor = info.Vendor;
            this.Architecture = info.Architecture;
            this.SpeedMax = info.SpeedMax;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        public virtual Guid? ExecutorId
        {
            get { return executorId; }
            protected set { executorId = value; }
        }

        protected internal virtual void UpdateSystem(ProcessorSystemInfo info)
        {
            this.Description = info.Description;
            this.Vendor = info.Vendor;
            this.Architecture = info.Architecture;
            this.SpeedMax = info.SpeedMax;
        }

        protected internal virtual void UpdatePerformance(ProcessorPerformanceInfo info)
        {
            this.SpeedCurrent = info.SpeedCurrent;
            this.LoadTotalCurrent = info.LoadTotalCurrent;
            this.LoadUsageCurrent = info.LoadUsageCurrent;
        }

        protected internal virtual void UpdateLimits(ProcessorLimitInfo info)
        {
            this.LoadUsageLimit = info.LoadUsageLimit;
        }

        public virtual string Name
        {
            get { return name; }
            protected set
            {
                this.name = value;
            }
        }

        public virtual string Description
        {
            get { return description; }
            protected set
            {
                this.description = value;
            }
        }

        public virtual string Vendor
        {
            get { return vendor; }
            protected set
            {
                this.vendor = value;
            }
        }

        public virtual Architecture Architecture
        {
            get { return architecture; }
            protected set
            {
                this.architecture = value;
            }
        }

        public virtual double SpeedMax
        {
            get { return speedMax; }
            protected set
            {
                this.speedMax = value;
            }
        }

        public virtual double SpeedCurrent
        {
            get { return speedCurrent; }
            protected set
            {
                this.speedCurrent = value;
            }
        }

        public virtual double LoadTotalCurrent
        {
            get { return loadTotalCurrent; }
            protected set
            {
                this.loadTotalCurrent = value;
            }
        }

        public virtual double LoadUsageCurrent
        {
            get { return loadUsageCurrent; }
            protected set
            {
                this.loadUsageCurrent = value;
            }
        }

        public virtual double LoadUsageLimit
        {
            get { return loadUsageLimit; }
            protected set
            {
                this.loadUsageLimit = value;
            }
        }

        #region ITransformable<ProcessorSystemInfo> Members

        ProcessorSystemInfo ITransformable<ProcessorSystemInfo>.Transform()
        {
            return new ProcessorSystemInfo(this.Name, 
                this.Description,
                this.Vendor, 
                this.Architecture,
                this.SpeedMax);
        }

        #endregion

        #region ITransformable<ProcessorPerformanceInfo> Members

        ProcessorPerformanceInfo ITransformable<ProcessorPerformanceInfo>.Transform()
        {
            return new ProcessorPerformanceInfo(this.Name, 
                this.SpeedCurrent, 
                this.LoadTotalCurrent, 
                this.LoadUsageCurrent);
        }

        #endregion

        #region ITransformable<ProcessorLimitInfo> Members

        ProcessorLimitInfo ITransformable<ProcessorLimitInfo>.Transform()
        {
            return new ProcessorLimitInfo(this.Name, this.LoadUsageLimit);
        }

        #endregion

        #region IUpdatable<ProcessorSystemInfo> Members

        void IUpdatable<ProcessorSystemInfo>.Update(ProcessorSystemInfo info)
        {
            if ((this as IUpdatable<ProcessorSystemInfo>).CanUpdate(info))
            {
                LoadFrom(info);
            }
        }

        #endregion

        #region IUpdatable<ProcessorSystemInfo> Members

        bool IUpdatable<ProcessorSystemInfo>.CanUpdate(ProcessorSystemInfo info)
        {
            return (info != null && (info.Name == this.Name));
        }

        #endregion
    }
}
