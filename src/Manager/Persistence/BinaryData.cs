using System;

namespace Utilify.Platform.Manager.Persistence
{
    [Serializable]
    public class BinaryData //this class needs to be public, so that it can be proxied by the persistence mapper. :/
    {
        private long id; //pk

        /// <summary>
        /// Gets or sets the id of this data object
        /// </summary>
        protected internal virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        private byte[] value;

        /// <summary>
        /// Creates a new instance of the BinaryData class
        /// </summary>
        protected BinaryData() { } //for persistence proxy

        public BinaryData(string path) : this()
        {
            //this has to be a local path
            if (path == null)
                throw new ArgumentNullException("path");
            if (path.Trim().Length == 0)
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "path");

            this.path = path;
        }

        /// <summary>
        /// Creates a new instance of the BinaryData class with the specified data value
        /// </summary>
        /// <param name="value">data value for the contents</param>
        public BinaryData(byte[] data) : this()
        {
            this.value = data;
        }

        /// <summary>
        /// Gets the byte[] representing the binary data
        /// </summary>
        /// <returns></returns>
        public virtual byte[] GetValue()
        {
            return value;
        }

        /// <summary>
        /// Gets the length (in bytes) of the binary data
        /// </summary>
        public virtual long Length
        {
            get
            {
                if (value != null)
                    return value.LongLength;
                else
                    return 0;
            }
        }

        //optional path to the file on disk, if this data object is stored on the file system
        private string path;
        public virtual string Path
        {
            get
            {
                return path;
            }            
        }
    }
}
