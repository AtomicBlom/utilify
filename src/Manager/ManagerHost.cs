using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Threading;

using Utilify.Platform.Manager.Services;
using Utilify.Platform.Manager.Persistence;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform.Manager
{
    public sealed class ManagerHost : IDisposable
    {
        //todoFindOut: find a better way to do this:
        //private bool enableRemotingHost = false; //this is needed only to support Mono executors - can be enabled in config.

        private readonly Logger logger = null;
        private List<PlatformServiceBase> backgroundServices = null;

        //private Thread dbCheck = null;
        private AutoResetEvent dsCheckWait = null;

        public event EventHandler<EventArgs> OnShutDown;

#if !MONO
        private List<ManagerServiceHost> listeners = null;
#endif

        //removed control app domain link demand to make it work on Azure
        public ManagerHost()
        {
            //listen to unhandled exceptions in this appdomain
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            logger = new Logger();

            string env = Environment.NewLine + Helper.GetEnvironment();
            logger.Info(env);

            ApplyConfiguration();

            //create background services
            backgroundServices = new List<PlatformServiceBase>();
            backgroundServices.Add(new Scheduler());

            //listen to error events from background services
            foreach (PlatformServiceBase service in backgroundServices)
            {
                service.Error += new EventHandler<ServiceErrorEventArgs>(InternalService_Error);
            }

        }

        private void ApplyConfiguration()
        {
            //logger.Debug("Enabling remoting clients: " + enableRemotingHost);
            //logger.Debug("Enabling wcf clients: " + enableWcfHost);
            //for the manager, we do a Remoting.Configure only if the 'enable remoting' is set
            //if (enableRemotingHost)
            //{
            //    string configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            //    if (configFile != null)
            //    {
            //        RemotingConfiguration.Configure(configFile, false); //let security be enabled in the config
            //    }
            //    else
            //    {
            //        throw new ConfigurationErrorsException("Could not configure remoting: could not find config file for current AppDomain!");
            //    }
            //}
#if !MONO
            listeners = ConfigurationHelper.CreateListeners();

            //listen to error events from listener services
            foreach (ServiceHost listener in listeners)
            {
#if DEBUG
                ConfigurationHelper.LogListenerConfig(listener);
#endif
                listener.Faulted += new EventHandler(Listener_Faulted);
            }
#endif
        }

        public void StartService()
        {
            VerifyDisposed();

            var gotConnection = VerifyDatabaseConnection();
            //dbCheck = new Thread(new ThreadStart(VerifyDatabaseConnection));
            //dbCheck.Name = "VerifyDataStoreThread";
            //dbCheck.IsBackground = true;
            //dbCheck.Start();
            //dbCheck.Join();

            if (!gotConnection)
            {
                OnShutDown(this, EventArgs.Empty);
                return;
            }
#if !MONO
            //based on the config property, this list may be empty, so nothing more to do here.
            foreach (ServiceHost listener in listeners)
            {
                logger.Info("Starting listener service : {0}", listener.Description.Name);
                listener.Open();
            }
#endif

            foreach (PlatformServiceBase service in backgroundServices)
            {
                logger.Info("Starting background service : {0}", service.Name);
                service.Start();
            }
        }

        private bool VerifyDatabaseConnection()
        {
            bool gotConnection = false;

            if (dsCheckWait == null)
                dsCheckWait = new AutoResetEvent(false);

            //check if we can connect to the database
            DataStore ds = DataStore.GetInstance();

            //5 retries, 5 seconds apart
            int interval = 5000; long maxRetries = 5;
            BackOffHelper helper = new BackOffHelper(interval, maxRetries);
            while (helper.IsWithinMaxLimits && !stopDbConnectionCheck)
            {
                try
                {
                    logger.Info("Verifying database connection...");
                    ds.CheckConnection();
                    gotConnection = true;
                    break;
                }
                catch (ObjectDisposedException)
                {
                    break;
                }
                catch (Exception ex)
                {
                    if (helper.IsWithinMaxLimits)
                    {
                        logger.Warn("Could not access database in attempt {0} of {1}: {2}. Will try again in {3} secs...",
                            helper.NumberOfRetries, helper.MaxRetries, ex.Message, helper.CurrentInterval / 1000);
                        logger.Debug("VerifyDatabaseConnection", ex);

                        dsCheckWait.WaitOne(helper.GetNextInterval());
                    }
                    else
                    {
                        logger.Warn("Could not connect to database. Not trying anymore...");
                    }
                }
            }

            return gotConnection;
        }

        public void StopService()
        {
            StopService(ServiceStopOptions.Default);
        }

        public void StopService(ServiceStopOptions options)
        {
            //first close all external connections.
#if !MONO
            //based on the config property, this list may be empty, so nothing more to do here.
            foreach (ServiceHost listener in listeners)
            {
                if (listener != null && listener.State != CommunicationState.Closed && listener.State != CommunicationState.Faulted)
                {
                    if (listener.State != CommunicationState.Created)
                        logger.Info("Stopping listener service @ {0}", listener.Description.Name);
                    listener.Close();
                }
            }
#endif

            foreach (PlatformServiceBase service in backgroundServices)
            {
                service.Stop(options); //this logs the stopping message inside it anyway
            }

            stopDbConnectionCheck = true; //in case we are still in the initial check to connect to the db
            if (dsCheckWait != null)
                dsCheckWait.Set(); //ask the thread to get out of its sleep
        }

        #region Error handling
        
        void Listener_Faulted(object sender, EventArgs e)
        {
            string senderName = (sender == null) ? "Unknown" : sender.GetType().Name;
            logger.Error("Error in listener : {0}.", senderName);

            //One way to deal with this would be to restart the service.
            //But since we don't know if it will still work, let's just bring the whole thing down!
            StopService();
        }

        void InternalService_Error(object sender, ServiceErrorEventArgs e)
        {
            string senderName = (sender == null) ? "Unknown" : sender.GetType().Name;
            logger.Error("Error in service : {0}. IsTerminating = {1}", e.Error, senderName, e.IsTerminating);
            
            //One way to deal with this would be to restart the service.
            //But since we don't know if it will still work, let's just bring the whole thing down!
            if (e.IsTerminating)
            {
                StopService();

                //avoid force process terminate. handle this more gracefully in the hosting process via an event
                EventHelper.RaiseEvent<EventArgs>(OnShutDown, this, EventArgs.Empty);
            }
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            logger.Error("Unhandled exception in current domain - {0}. IsTerminating = {1}, Exception = {2}",
                AppDomain.CurrentDomain.FriendlyName, e.IsTerminating, e.ExceptionObject.ToString());

            EventHelper.RaiseEvent<EventArgs>(OnShutDown, this, EventArgs.Empty);
        }

        #endregion

        #region IDisposable Members
        private bool disposed = false;
        private bool stopDbConnectionCheck;

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                //clean up managed stuff

                //try to close down all db connections
                try
                {
                    DataStore ds = DataStore.GetInstance();
                    (ds as IDisposable).Dispose();
                }
                catch (Exception ex)
                {
                    logger.Debug("Error closing db connections: " + ex.ToString());
                }

#if MONO
                //close down any remoting connections
                RemotingHelper.ShutdownChannels();
#endif
            }

            //we have no base class to call dispose on, so nothing more to do here.

            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
        #endregion
    }

}
