using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Linux
{
    internal static class ProcessorInfoUtil
    {
        private static readonly Logger logger = new Logger();

        private const string cpuInfoFile = "/proc/cpuinfo";

        private const string processor = "processor";
        private const string vendor_id = "vendor_id";
        private const string cpu_MHz = "cpu MHz";
        private const string model_name = "model name";

        /*
            Expected /proc/cpuinfo format:

            processor	: 0
            vendor_id	: GenuineIntel
            cpu family	: 6
            model		: 13
            model name	: Intel(R) Pentium(R) M processor 2.13GHz
            stepping	: 8
            cpu MHz		: 2130.488
            cache size	: 2048 KB
            fdiv_bug	: no
            hlt_bug		: no
            f00f_bug	: no
            coma_bug	: no
            fpu		    : yes
            fpu_exception	: yes
            cpuid level	: 2
            wp		    : yes
            flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat clflush dts acpi mmx fxsr sse sse2 ss nx up
            bogomips	: 4270.39

         */

        public static ProcessorSystemInfo[] GetProcessorInfo()
        {
            // Get Processor Architecture. All CPUs on the system should have the same architecture.
            Architecture arch = GetProcessorArchitecture();

            //read and parse the cpu info file
            List<NameValueCollection> procs = ReadCpuInfo();

            // Get the list of Processors
            List<ProcessorSystemInfo> infos = new List<ProcessorSystemInfo>(procs.Count);
            foreach (NameValueCollection proc in procs)
            {
                string name = proc.Get(processor) ?? "Unknown";
                string description = proc.Get(model_name) ?? "Unknown";
                string vendor = proc.Get(vendor_id) ?? "Unknown";
                double speedMax = 0;
                Double.TryParse(proc.Get(cpu_MHz), out speedMax);

                ProcessorSystemInfo info = new ProcessorSystemInfo(name, description, vendor, Architecture.X86, speedMax);
                infos.Add(info);
            }

            return infos.ToArray();
        }

        private static List<NameValueCollection> ReadCpuInfo()
        {
            List<NameValueCollection> procs = new List<NameValueCollection>();
            try
            {
                using (FileStream cpuInfoStream = File.OpenRead(cpuInfoFile))
                {
                    using (StreamReader sr = new StreamReader(cpuInfoStream))
                    {
                        string line = null;

                        NameValueCollection proc = new NameValueCollection();
                        while (!sr.EndOfStream)
                        {
                            line = sr.ReadLine();
                            if (line.Trim().Length > 0)
                            {
                                //parse it:
                                string[] tokens = line.Split(new string[] { ":" }, StringSplitOptions.None);
                                if (tokens != null && tokens.Length == 2)
                                {
                                    string name = tokens[0].Trim();
                                    string value = tokens[1].Trim();
                                    proc.Add(name, value);
                                }
                            }
                            else
                            {
                                //add the proc to the list
                                procs.Add(proc);
                                proc = new NameValueCollection();
                            }
                        }
                    }
                }
            }
            catch (IOException iox)
            {
                logger.Warn("Error reading cpu info from " + cpuInfoFile + Environment.NewLine + iox.ToString());
            }

            return procs;
        }

        public static Architecture GetProcessorArchitecture()
        {
           //todoMono: for now just return x86. but later read the value from the system
            return Architecture.X86;
        }

        public static int GetProcessorCount()
        {
            return System.Environment.ProcessorCount;
        }

        public static ProcessorPerformanceInfo[] GetProcessorPerfInfo()
        {
            //todoMono: implement proc performance info for Linux
            ProcessorPerformanceInfo[] perfInfos = new ProcessorPerformanceInfo[1];
            perfInfos[0] = new ProcessorPerformanceInfo("Processor1", 2, 0, 0);
            return perfInfos;
        }
    }
}
