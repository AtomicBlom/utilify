﻿using System;
using System.Windows.Forms;
using Utilify.Platform;

namespace Utilify.Framework.UI
{
    /// <summary>
    /// Represents the LoginForm UI component used for getting user input for connecting to a service on the Manager.
    /// </summary>
    public partial class LoginForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginForm"/> class.
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();

            view.Connecting += view_Connecting;
            view.Cancelling += view_Cancelling;
        }

        void view_Cancelling(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel; //closes the form (with a Cancel result)
        }

        void view_Connecting(object sender, ConnectionEventArgs<ConnectionSettings> e)
        {
            this.settings = e.ConnectionData;
            this.DialogResult = DialogResult.OK;
        }

        private ConnectionSettings settings = null;
        /// <summary>
        /// Gets the connection settings.
        /// </summary>
        /// <value>The connection settings.</value>
        public Utilify.Platform.ConnectionSettings ConnectionSettings
        {
            get
            {
                return this.settings;
            }
        }
    }
}
