package com.utilify.platform.test;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.JobStatusEvent;
import com.utilify.framework.JobStatusListener;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;

public class AdditionTestProgram implements JobStatusListener, ErrorListener {

    private static Logger logger = Logger.getLogger(AdditionTestProgram.class.getName());

    public static void main(String[] args) throws IOException, 
    	ApplicationInitializationException, InterruptedException {

    	Logger rootLogger = Logger.getLogger("");
    	Handler[] handlers = rootLogger.getHandlers();
    	for (int i = 0; i < handlers.length; i++){
    		if (handlers[i] instanceof ConsoleHandler){
    			handlers[i].setLevel(Level.ALL);
    		}
    	}
    	
        logger.info("Started logging...");

        AdditionTestProgram prog = new AdditionTestProgram();
        prog.test();
        
        System.out.println("Press enter to exit...");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }

    private void test() throws ApplicationInitializationException, InterruptedException{
    	logger.fine("Starting client api test...");

        ArrayList<Job> remoteJobs = new ArrayList<Job>();

        Application app = new Application("My Addition Application");

        for (int i = 0; i < 10; i++) {
            AdditionJob job = new AdditionJob(i, i);
            Job remoteJob = app.addJob(job);
            remoteJobs.add(remoteJob);
        }

        app.addErrorListener(this); 
        
        logger.info("Submitting Application");

        app.start();

        for (Job remoteJob : remoteJobs) {
            logger.fine("Waiting for Job to complete. " + remoteJob.getId());
            remoteJob.waitForCompletion();
            logger.info("Job completed.");
            AdditionJob myJob = (AdditionJob)remoteJob.getCompletedInstance();
            logger.info(String.format("Are my sums right? %d + %d = %d", myJob.getValueA(), myJob.getValueB(), myJob.getResult()));
            logger.info(String.format("Job %s executed in %d ms.", remoteJob.getId(), 
            		(remoteJob.getFinishTime().getTime() - remoteJob.getStartTime().getTime())));
            //remoteJob.
        }
    }
    
	public void onJobStatusChanged(JobStatusEvent event) {
	}

	public void onError(ErrorEvent event) {
		System.out.println("I blew up: \n" +  event.getException().toString());
		if (event.getException().getCause() != null)
			System.out.print("Cause: ");
			event.getException().getCause().printStackTrace();
	}
}
