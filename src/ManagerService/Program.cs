using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using log4net;

namespace Utilify.Platform.Manager
{
    static class Program 
    {
        private static ILog log; 

        //todoLater: //todoConfig //Do we need a UI for configuring the config options for the WCF services (need to (un-)register http acls when needed)?
        //Do we need a UI section for configuring log4net?
        //Another UI section for configuring DB options: connection string etc
        //Additional config options:?
        // - Default dependency cache (currently it is in the db, perhaps we could have additional options to put it in a network share)
        // - Default log file location
        // - Scheduler poll interval, other schedule params such as algorithm etc : need to check what else is needed.

	    //LocalService account does not exist in Windows2000 - Windows 2000 is not supported.

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //the unhandled exception handler is set here, 
            //since the Main does a lot more things that can cause errors.
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(DefaultErrorEventHandler);

            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            log = log4net.LogManager.GetLogger(typeof(Program));
#if DEBUG
            System.Diagnostics.Trace.Listeners.Add(new Log4netTraceListener(log));
#endif

            Logger.MessageLogged += new EventHandler<LogEventArgs>(Logger_MessageLogged_Log4Net);

            string opt = null;
            if (args.Length > 0)
            {
                opt = args[0];
                if (opt != null)
                    opt = opt.Trim();
            }

            if (opt != null && opt.Equals("/install", StringComparison.CurrentCultureIgnoreCase))
            {
                //install the service
                InstallService();
            }
            else if (opt != null && opt.Equals("/uninstall", StringComparison.CurrentCultureIgnoreCase))
            {
                //uninstall the service
                UninstallService();
            }
            else if ("/console".Equals(opt, StringComparison.CurrentCultureIgnoreCase))
            {
                ManagerService ms = new ManagerService();

                var method = (typeof(ManagerService)).GetMethod("OnStart", BindingFlags.NonPublic | BindingFlags.Instance);
                method.Invoke(ms, new[] { args });

                Console.WriteLine("Press any key to stop");
                Console.ReadKey();

                method = (typeof(ManagerService)).GetMethod("OnStop", BindingFlags.NonPublic | BindingFlags.Instance);
                method.Invoke(ms, null);

            }
            else
            {
                //start the service
                ManagerService ms = new ManagerService();
                //if (!WindowsServiceHelper.CheckServiceInstallation(ms.ServiceName))
                //{
                //    InstallService();
                //}

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { ms };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static void DefaultErrorEventHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            log.Fatal("Application terminating = " + args.IsTerminating + ". Fatal unhandled error from " + sender,
                         ex);
        }

        private static void InstallService()
        {
#if !MONO
            string path = string.Format("/assemblypath={0}", Assembly.GetExecutingAssembly().Location);
            WindowsServiceHelper.InstallService(new ProjectInstaller(), path);
#endif
        }

        private static void UninstallService()
        {
#if !MONO
            string path = string.Format("/assemblypath={0}", Assembly.GetExecutingAssembly().Location);
            WindowsServiceHelper.UninstallService(new ProjectInstaller(), path);
#endif
        }

        //unfortunately, we have some conditional compilation here: but it is only for limiting logging related stuff.
        //captures all framework log messages
        private static void Logger_MessageLogged_Log4Net(object sender, LogEventArgs e)
        {
            string format = "<{0}:{1}> {2} - {3} {4}";
            string filename = Path.GetFileName(e.StackFrame.GetFileName());
            string lineNumber = e.StackFrame.GetFileLineNumber().ToString();
            string methodName = e.StackFrame.GetMethod().Name;

            switch (e.Level)
            {
                case LogLevel.Debug:
                    log.DebugFormat(format,
                        filename, lineNumber, methodName,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Info:
                    log.InfoFormat(format,
                        filename, lineNumber, methodName,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Warn:
                    log.WarnFormat(format,
                        filename, lineNumber, methodName,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Error:
                    log.ErrorFormat(format,
                        filename, lineNumber, methodName,
                        e.Message, e.Exception);
                    break;
            }
           
        }
    }
}