﻿using Utilify.Platform.Security;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
#endif
    public interface IUserManager : IManagerService
    {

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        UserInfo Authenticate();

#if !MONO
        [OperationContract]
        //[FaultContract(typeof(EntityNotFoundFault))]
        //[FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        UserInfo[] GetUsers();

#if !MONO
        [OperationContract]
        //[FaultContract(typeof(EntityNotFoundFault))]
        //[FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        GroupInfo[] GetGroups();

#if !MONO
        [OperationContract]
        //[FaultContract(typeof(EntityNotFoundFault))]
        //[FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        PermissionInfo[] GetPermissions();

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        void UpdateUser(UserInfo userInfo);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        void CreateUser(UserInfo userInfo);
    
    }
}
