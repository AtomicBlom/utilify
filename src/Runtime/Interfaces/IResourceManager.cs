using Utilify.Platform.Contract;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
    //to use Xml Serializer : [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
#endif
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true, Namespace = Namespaces.ManagerCompat)]
    public interface IResourceManager : IManagerService
    {

#if !MONO
        [OperationContract]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        SystemInfo[] GetExecutors();

#if !MONO
        [OperationContract]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        PerformanceInfo[] GetSystemPerformance();

    }
}

