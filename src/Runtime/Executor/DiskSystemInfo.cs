using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DiskSystemInfo
    {

        private string root;
        private FileSystemType fileSystem;
        private long sizeTotal; //(in bytes)

        private DiskSystemInfo() { }

        /// <summary>
        /// Creates an instance of DiskSystemInfo
        /// </summary>
        /// <param name="root"></param>
        /// <param name="fileSystem"></param>
        /// <param name="sizeTotal"></param>
        public DiskSystemInfo(string root, FileSystemType fileSystem, long sizeTotal) : this()
        {
            this.Root = root;
            this.FileSystem = fileSystem;
            this.SizeTotal = sizeTotal;
        }

        /// <summary>
        /// Gets the Root for this disk.
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each disk on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Root
        {
            get { return root; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("root");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Disk root cannot be empty", "root");
                root = value;
            }
        }

        /// <summary>
        /// Gets the type of the File System on this disk.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public FileSystemType FileSystem
        {
            get { return fileSystem; }
            private set
            {
                fileSystem = value;
            }
        }

        /// <summary>
        /// Gets the total size (in bytes) for this disk.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeTotal
        {
            get { return sizeTotal; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero", "sizeTotal");
                sizeTotal = value;
            }
        }

        /// <summary>
        /// Gets the string representation for this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} : {2} bytes Total",
                root, fileSystem, sizeTotal);
        }
    }
}
