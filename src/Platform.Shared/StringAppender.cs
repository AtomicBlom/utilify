using System;
using System.Text;

namespace Utilify.Platform
{
    //todoLater: we can make this a nice XML-based data-structure: with richer information such as events that occured during a job execution, timestamp for 
    //each event, type of event, and additional information.
    /// <summary>
    /// Represents a write-only version of the <see cref="System.Text.StringBuilder"/> class.
    /// </summary>
    [Serializable]
    public class StringAppender
    {
        private StringBuilder builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringAppender"/> class.
        /// </summary>
        public StringAppender()
        {
            builder = new StringBuilder();
        }

        /// <summary>
        /// Appends the specified value to the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public StringAppender Append(string value)
        {
            builder.Append(value);
            return this;
        }

        /// <summary>
        /// Appends the specified value followed by a new line character to the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public StringAppender AppendLine(string value)
        {
            builder.AppendLine(value);
            return this;
        }

        /// <summary>
        /// Appends a new line character to the string.
        /// </summary>
        /// <returns></returns>
        public StringAppender AppendLine()
        {
            builder.AppendLine();
            return this;
        }

        /// <summary>
        /// Appends the formatted string.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        public StringAppender AppendFormat(string format, params object[] args)
        {
            builder.AppendFormat(format, args);
            return this;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// This includes the entire string appended to this instance so far.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return builder.ToString();
        }
    }
}
