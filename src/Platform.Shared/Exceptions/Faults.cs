#if !MONO

using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Utilify.Platform
{
    //todoDoc

    /// <summary>
    /// Represents the SOAP fault that occurs when there was a server-side exception.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ServerFault //to represent a server-side error
    {
        private string message;
        private string description;

        private ServerFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerFault"/> class.
        /// </summary>
        /// <param name="message">The fault message.</param>
        /// <param name="description">The fault description.</param>
        public ServerFault(string message, string description)
            : this()
        {
            this.Message = message;
            this.Description = description;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set 
            {
                if (value == null) value = string.Empty; //let's not validate this - its already describing a fault!
                message = value; 
            }
        }

        /// <summary>
        /// Gets the fault description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(IsRequired = true)]
        public string Description
        {
            get { return description; }
            private set 
            {
                if (value == null) value = string.Empty; //let's not validate this - its already describing a fault!
                description = value;
            }
        }
    }

    //use this wherever invalid ids/file or directory names are passed and the entity is not found
    /// <summary>
    /// Represents the SOAP fault that occurs when an entity (such as an application, job, etc) 
    /// </summary>
    [Serializable]
    [DataContract]
    public class EntityNotFoundFault
    {
        private string message;
        private string entityId;
        private EntityType entityType;

        private EntityNotFoundFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityNotFoundFault"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityType">Type of the entity.</param>
        public EntityNotFoundFault(string message, string entityId, EntityType entityType) : this()
        {
            this.Message = message;
            this.EntityId = entityId;
            this.EntityType = entityType;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set
            {
                if (value == null) value = string.Empty;
                message = value;
            }
        }

        /// <summary>
        /// Gets the entity id value that could not be found
        /// </summary>
        [DataMember(IsRequired = true)]
        public string EntityId
        {
            get
            {
                return entityId;
            }
            private set
            {
                if (value == null) value = string.Empty;
                entityId = value;
            }
        }

        /// <summary>
        /// Gets the type of entity
        /// </summary>
        [DataMember(IsRequired = true)]
        public EntityType EntityType
        {
            get { return entityType; }
            private set { entityType = value; }
        }
    }

    /// <summary>
    /// Represents the SOAP fault that occurs when an invalid argument is passed to an operatin on a remote service.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ArgumentFault
    {
        private string message;
        private string parameterName;
        private bool isValueNull;

        private ArgumentFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentFault"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="isValueNull">if set to <c>true</c> [is value null].</param>
        public ArgumentFault(string message, string parameterName, bool isValueNull)
            : this()
        {
            this.Message = message;
            this.ParameterName = parameterName;
            this.IsValueNull = isValueNull;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set
            {
                if (value == null) value = string.Empty;
                message = value;
            }
        }

        /// <summary>
        /// Gets the name of the parameter with the invalid value.
        /// </summary>
        /// <value>The name of the parameter.</value>
        [DataMember(IsRequired = true)]
        public string ParameterName
        {
            get { return parameterName; }
            private set
            {
                if (value == null) value = string.Empty;
                parameterName = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the argument value is null.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the argument value is null; otherwise, <c>false</c>.
        /// </value>
        [DataMember(IsRequired = true)]
        public bool IsValueNull
        {
            get { return isValueNull; }
            private set { isValueNull = value; }
        }
    }

    /// <summary>
    /// Represents the SOAP fault that occurs when an operation is invoked on the remote service that is invalid for the current context.
    /// </summary>
    [Serializable]
    [DataContract]
    public class InvalidOperationFault
    {
        private string message;

        private InvalidOperationFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidOperationFault"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidOperationFault(string message)
            : this()
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set
            {
                if (value == null) value = string.Empty;
                message = value;
            }
        }
    }

    /// <summary>
    /// Represents a SOAP fault that occurs when authentication of a request fails.
    /// </summary>
    [Serializable]
    [DataContract]
    public class AuthenticationFault
    {
        private string message;

        private AuthenticationFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationFault"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public AuthenticationFault(string message)
            : this()
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set
            {
                if (value == null) value = string.Empty;
                message = value;
            }
        }
    }

    /// <summary>
    /// Represents the SOAP fault that occurs when an unauthorized request is received.
    /// </summary>
    [Serializable]
    [DataContract]
    public class AuthorizationFault
    {
        private string message;

        private AuthorizationFault() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationFault"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public AuthorizationFault(string message)
            : this()
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = true)]
        public string Message
        {
            get { return message; }
            private set
            {
                if (value == null) value = string.Empty;
                message = value;
            }
        }
    }

}

#endif
