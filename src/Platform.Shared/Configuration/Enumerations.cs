namespace Utilify.Platform
{
    /// <summary>
    /// Indicates the type of transport protocol used for communication
    /// </summary>
    public enum CommunicationProtocol
    {
        /// <summary>
        /// The Tcp transport is used.
        /// </summary>
        Tcp = 0,
        /// <summary>
        /// The Http or secure Http transport is used.
        /// </summary>
        Http = 1,
        //BasicHttp = 2
    }

    /// <summary>
    /// Indicates the type of credential used to authenticate to a service.
    /// </summary>
    public enum CredentialType
    {
        /// <summary>
        /// Specifies authentication using a username and password.
        /// </summary>
        Username = 0,
        /// <summary>
        /// Specifies authentication using windows credentials.
        /// </summary>
        Windows = 1,
        //Certificate = 2,
        /// <summary>
        /// Specifies authentication using anonymous credentials.
        /// </summary>
        None = 3
    }

    /// <summary>
    /// Indicates the type of service.
    /// </summary>
    public enum ServiceType
    {
        /// <summary>
        /// The default service type (the interpretation of default differs depending on context).
        /// </summary>
        Default = 0,
        /// <summary>
        /// The application manager service.
        /// </summary>
        ApplicationManager = 1,
        /// <summary>
        /// The job manager service.
        /// </summary>
        JobManager = 2,
        /// <summary>
        /// The workload manager service.
        /// </summary>
        WorkloadManager = 3,
        /// <summary>
        /// The resource manager service.
        /// </summary>
        ResourceManager = 4,
        /// <summary>
        /// The resource monitor service.
        /// </summary>
        ResourceMonitor = 5,
        /// <summary>
        /// The data transfer service.
        /// </summary>
        DataTransferService = 6,
        /// <summary>
        /// The user manager service.
        /// </summary>
        UserManager = 7
    }
}