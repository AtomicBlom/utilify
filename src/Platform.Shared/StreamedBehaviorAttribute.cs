using System;

namespace Utilify.Platform
{
    /// <summary>
    /// Specifies that a service (class or interface) decorated with this attribute requires or supports data streaming.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public sealed class StreamedBehaviorAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreamedBehaviorAttribute"/> class.
        /// </summary>
        public StreamedBehaviorAttribute(){ }
    }
}
