using System.Reflection;
using Utilify.Platform;

[assembly: AssemblyCompany(Constants.CompanyName)]
[assembly: AssemblyProduct(Constants.ProductName)]
[assembly: AssemblyCopyright(Constants.Copyright)]
[assembly: AssemblyTrademark(Constants.Trademarks)]

[assembly: AssemblyCulture("")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

//This shows up as Product Version in Windows Explorer
//We make this the same for all files in a particular product version. And increment it globally for all projects.
//We then use this as the Product Version in Wix installers as well.
[assembly: AssemblyInformationalVersion("1.0.0.0")]

