#if !MONO
using System;
using System.Collections.Generic;
using System.ServiceModel;
using Utilify.Platform.Contract;

namespace Utilify.Platform
{
    internal partial class WorkloadManagerProxy : WCFProxyBase<IWorkloadManager>, IWorkloadManager
    {       

        #region Constructors
        public WorkloadManagerProxy() { }

        public WorkloadManagerProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {            
        }

        #endregion

        #region IWorkloadManager Members

        ApplicationSubmissionInfo[] IWorkloadManager.GetApplications()
        {
            List<ApplicationSubmissionInfo> infos = new List<ApplicationSubmissionInfo>();
            try
            {
                infos.AddRange(base.Channel.GetApplications());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        JobInfo[] IWorkloadManager.GetJobs(string applicationId)
        {
            List<JobInfo> infos = new List<JobInfo>();
            try
            {
                infos.AddRange(base.Channel.GetJobs(applicationId));
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        void IWorkloadManager.AbortApplications(string[] applicationIds)
        {
            try
            {
                base.Channel.AbortApplications(applicationIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IWorkloadManager.AbortJobs(string[] jobIds)
        {
            try
            {
                base.Channel.AbortJobs(jobIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IWorkloadManager.ResetJobs(string[] jobIds)
        {
            try
            {
                base.Channel.ResetJobs(jobIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IWorkloadManager.PauseApplications(string[] applicationIds)
        {
            try
            {
                base.Channel.PauseApplications(applicationIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        ApplicationStatusInfo[] IWorkloadManager.GetApplicationStatus(string[] applicationIds)
        {
            List<ApplicationStatusInfo> infos = new List<ApplicationStatusInfo>();
            try
            {
                infos.AddRange(base.Channel.GetApplicationStatus(applicationIds));
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        JobStatusInfo[] IWorkloadManager.GetJobStatus(string[] jobIds)
        {
            List<JobStatusInfo> infos = new List<JobStatusInfo>();
            try
            {
                infos.AddRange(base.Channel.GetJobStatus(jobIds));
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        JobStatusInfo[] IWorkloadManager.GetJobStatusForApplication(string applicationId)
        {
            List<JobStatusInfo> infos = new List<JobStatusInfo>();
            try
            {
                infos.AddRange(base.Channel.GetJobStatusForApplication(applicationId));
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        void IWorkloadManager.ResumeApplications(string[] applicationIds)
        {
            try
            {
                base.Channel.ResumeApplications(applicationIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion
    }
}
#endif