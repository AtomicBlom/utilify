﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Utilify.Platform
{
    internal static class ValidationHelper
    {
        internal static void CheckNull<T>(T parameter, string paramName) where T : class
        {
            paramName = paramName ?? string.Empty;
            if (parameter == null)
                ThrowHelper.ArgumentNull(paramName);
        }
    }

    internal static class ThrowHelper
    {
        internal static void ArgumentNull(string paramName)
        {
            paramName = paramName ?? string.Empty;
            throw new ArgumentNullException(paramName);            
        }

        internal static void ArgumentException(string message, string paramName)
        {
            if (message == null || message.Trim().Length == 0)
                message = Errors.ExceptionOccured;

            if (paramName != null && paramName.Trim().Length > 0)
                throw new ArgumentException(message, paramName);
            else
                throw new ArgumentException(message);
        }

        internal static string GetMessage(string messageName)
        {
            if (messageName == null || messageName.Trim().Length == 0)
            {
                messageName = Errors.UnknownException;
            }

            string message = Errors.ResourceManager.GetString(messageName);
            return message;
        }

        internal static void ConfigurationException(string message)
        {
            throw new System.Configuration.ConfigurationErrorsException(message);
        }
    }
}
