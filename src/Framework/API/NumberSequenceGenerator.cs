using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Framework
{
    internal class NumberSequenceGenerator
    {
        private object syncLock = new object();
        private ulong sequence = 0;
        /// <summary>
        /// Gets the next number in sequence.
        /// This method is thread-safe.
        /// </summary>
        /// <returns></returns>
        internal ulong GetNext()
        {
            lock (syncLock)
            {
                if (sequence < ulong.MaxValue)
                    sequence++;
                else
                    sequence = 0; //reset the sequence
            }
            return sequence;
        }

        ///// <summary>
        ///// Gets the next sequence number.
        ///// The numbers generated are actually 64-bit unsigned integers.
        ///// This method is thread-safe.
        ///// </summary>
        ///// <returns></returns>
        //public string GetNextSequence()
        //{
        //    return "" + GetNext();
        //}
        
    }
}
