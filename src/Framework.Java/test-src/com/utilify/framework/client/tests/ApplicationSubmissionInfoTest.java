package com.utilify.framework.client.tests;

import org.junit.Assert;
import org.junit.Test;

import com.utilify.framework.client.ApplicationSubmissionInfo;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.QosInfo;

public class ApplicationSubmissionInfoTest {

	/*         [Category("ApplicationSubmissionInfo")]
     */
	
	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullNameCtor1(){
		new ApplicationSubmissionInfo(null, "me");
    }
	
	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullNameCtor2() {
		new ApplicationSubmissionInfo(null, "me", new DependencyInfo[] { });
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullNameCtor3() {
		new ApplicationSubmissionInfo(null, "me", new DependencyInfo[] { }, QosInfo.getDefault());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyNameCtor1() {
		new ApplicationSubmissionInfo(" ", "a");
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyNameCtor2() {
		new ApplicationSubmissionInfo(" ", "a", new DependencyInfo[] { });
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyNameCtor3() {
		new ApplicationSubmissionInfo(" ", "a", new DependencyInfo[] { }, QosInfo.getDefault());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullUserNameCtor1() {
		new ApplicationSubmissionInfo("aName", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullUserNameCtor2() {
		new ApplicationSubmissionInfo("aName", null, new DependencyInfo[] { });
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoNullUserNameCtor3() {
		new ApplicationSubmissionInfo("aName", null, new DependencyInfo[] { }, QosInfo.getDefault());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyUserNameCtor1() {
		new ApplicationSubmissionInfo("aName", " ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyUserNameCtor2() {
		new ApplicationSubmissionInfo("aName", " ", new DependencyInfo[] { });
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAppInfoEmptyUserNameCtor3() {
		new ApplicationSubmissionInfo("aName", " ", new DependencyInfo[] { }, QosInfo.getDefault());
	}

	@Test
	public void ApplicationSubmissionInfoCtor1() {
		ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo("aName", "me");
		// make sure dependencies array is internally set to a non-null value : an empty array
		DependencyInfo[] deps = appInfo.getDependencies();
		Assert.assertNotNull("dependencies array should be internally set to a non-null value", deps);
		Assert.assertEquals("dependencies array should be an empty array", 0, deps.length);
	}

	@Test
	public void ApplicationSubmissionInfoCtor2() {
		ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo("aName", "me", null);
		// even though we explicitly pass in null, the array should be set to a non-null value internally.
		// this is for best WS interop scenarios
		// test for that here:
		// make sure dependencies array is internally set to a non-null value : an empty array
		DependencyInfo[] deps = appInfo.getDependencies();
		Assert.assertNotNull("dependencies array should be internally set to a non-null value", deps);
		Assert.assertEquals("dependencies array should be an empty array", 0, deps.length);
	}

	@Test
	public void ApplicationSubmissionInfoCtor3() {
		ApplicationSubmissionInfo appInfo =                 new ApplicationSubmissionInfo("aName", "me", null, QosInfo.getDefault());
		// even though we explicitly pass in null, the array should be set to a non-null value internally.
		// this is for best WS interop scenarios
		// test for that here:
		// make sure dependencies array is internally set to a non-null value : an empty array        DependencyInfo[] deps = appInfo.getDependencies();
		Assert.assertNotNull("dependencies array should be internally set to a non-null value", deps);
		Assert.assertEquals("dependencies array should be an empty array", 0, deps.length);
	}
}