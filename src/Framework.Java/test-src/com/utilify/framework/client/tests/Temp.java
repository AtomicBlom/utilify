/**
 * 
 */
package com.utilify.framework.client.tests;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import com.utilify.framework.Helper;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.dependency.DependencyResolver;

/**
 * 
 */
public class Temp {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Detecting dependencies for temp class");
		long start = System.currentTimeMillis();
		Collection<DependencyInfo> deps = DependencyResolver.detectDependencies(junit.framework.Assert.class);
		long now = System.currentTimeMillis();
		System.out.println(String.format("\n\n*********************** Done in %d ms.", (now - start)));
		for (DependencyInfo dep : deps){
			System.out.println("Dep found : " + dep.getName());
		}
		
//		File bin = new File("bin");
//		System.out.println("current dir " + bin.getAbsolutePath());
//		Helper.jarDirectory(bin);

		//File bin = new File("bin");
		//System.out.println("current dir " + bin.getAbsolutePath());
		//Helper.jarDirectory(bin);
		
		
//		Helper.zipDirectory(bin);
//		System.out.println("Starting temp test ... ");
//		ApplicationManager proxy = new ApplicationManager();
//		IApplicationManager manager = proxy.getApplicationManagerDefaultEndPoint();
//		System.out.println("Got proxy");
//		ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo("app1", "me");
//		String appId = manager.submitApplication(appInfo);
//		System.out.println("SUBMITTED application - got id = " + appId);
	}

}
