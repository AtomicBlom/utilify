package com.utilify.framework.client.tests;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.utilify.framework.JobType;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.JobSubmissionInfo;
import com.utilify.framework.client.ResultInfo;
import com.utilify.framework.tests.TestHelper;

	/**
	* ranslator: Uses metadata: [TestFixture]
	* Class converted from .NET code
	**/

public class JobSubmissionInfoTest {

	/*
	 * [Category("JobSubmissionInfo")]
	 */
	
	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullAppIdCtor1() {
		new JobSubmissionInfo(null, JobType.CODE_MODULE, 
            TestHelper.getJobInstance(new TestJob()));
    }
	
	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullAppIdCtor2() {
		new JobSubmissionInfo(null, JobType.CODE_MODULE, 
	            TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>());
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullAppIdCtor3() {
		new JobSubmissionInfo(null, JobType.CODE_MODULE, 
	            TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>(), new ArrayList<ResultInfo>());
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoEmptyAppIdCtor1() {
		new JobSubmissionInfo(" ", JobType.CODE_MODULE, 
	            TestHelper.getJobInstance(new TestJob()));
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoEmptyAppIdCtor2() {
		new JobSubmissionInfo(" ", JobType.CODE_MODULE, 
	            TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>());
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoEmptyAppIdCtor3() {
		new JobSubmissionInfo(" ", JobType.CODE_MODULE, 
	            TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>(), new ArrayList<ResultInfo>());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullInstanceCtor1() {
		new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE, null);
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullInstanceCtor2() {
		new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE, null, new ArrayList<DependencyInfo>());
	}

	@Test (expected = IllegalArgumentException.class)
	public void createJobSubmissionInfoNullInstanceCtor3() {
		new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE, null, new ArrayList<DependencyInfo>(), new ArrayList<ResultInfo>());
	}

	@Test
	public void JobSubmissionInfoCtor1() {
		// correct instantiation
		JobSubmissionInfo jobInfo = new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE,
				TestHelper.getJobInstance(new TestJob()));
		Assert.assertNotNull("Application id should not be null", jobInfo.getApplicationId());
		DependencyInfo[] deps = jobInfo.getDependencies();
		Assert.assertNotNull("GetDependencies should not return null", deps);
		Assert.assertEquals("GetDependencies should return an empty array", 0, deps.length);
		ResultInfo[] res = jobInfo.getExpectedResults();
		Assert.assertNotNull("GetExpectedResults should not return null", res);
		Assert.assertEquals("GetExpectedResults should return an empty array", 0, res.length);
	}

	@Test
	public void JobSubmissionInfoCtor2() {
		// correct instantiation
		JobSubmissionInfo jobInfo = new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE,
				TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>());
		Assert.assertNotNull("Application id should not be null", jobInfo.getApplicationId());
		DependencyInfo[] deps = jobInfo.getDependencies();
		Assert.assertNotNull("GetDependencies should not return null", deps);
		Assert.assertEquals("GetDependencies should return an empty array", 0, deps.length);
		ResultInfo[] res = jobInfo.getExpectedResults();
		Assert.assertNotNull("GetExpectedResults should not return null", res);
		Assert.assertEquals("GetExpectedResults should return an empty array", 0, res.length);
	}

	@Test
	public void JobSubmissionInfoCtor3() {
		// correct instantiation
		JobSubmissionInfo jobInfo = new JobSubmissionInfo("SomeAppId", JobType.CODE_MODULE,
				TestHelper.getJobInstance(new TestJob()), new ArrayList<DependencyInfo>(), new ArrayList<ResultInfo>());
		Assert.assertNotNull("Application id should not be null", jobInfo.getApplicationId());
		DependencyInfo[] deps = jobInfo.getDependencies();
		Assert.assertNotNull("GetDependencies should not return null", deps);
		Assert.assertEquals("GetDependencies should return an empty array", 0, deps.length);
		ResultInfo[] res = jobInfo.getExpectedResults();
		Assert.assertNotNull("GetExpectedResults should not return null", res);
		Assert.assertEquals("GetExpectedResults should return an empty array", 0, res.length);
	}
}