
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDependencyStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDependencyStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DependencyStatusInfo" type="{http://schemas.utilify.com/2007/09/Client}DependencyStatusInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDependencyStatusInfo", propOrder = {
    "dependencyStatusInfos"
})
public class ArrayOfDependencyStatusInfo {

    @XmlElement(name = "DependencyStatusInfo", nillable = true)
    private List<DependencyStatusInfo> dependencyStatusInfos;

    public ArrayOfDependencyStatusInfo(){
    	dependencyStatusInfos = new ArrayList<DependencyStatusInfo>();
    }
    
    public ArrayOfDependencyStatusInfo(Collection<DependencyStatusInfo> statuses){
    	this();
    	if (statuses != null){
    		dependencyStatusInfos.addAll(statuses);
    	}
    }
    
    public ArrayOfDependencyStatusInfo(DependencyStatusInfo[] statuses){
    	this();
    	if (statuses != null){
    		for (int i = 0; i < statuses.length; i++) {
    			dependencyStatusInfos.add(statuses[i]);
			}
    	}
    }
    
    /**
     * Gets an array of DependencyStatusInfos.
     */    
    public DependencyStatusInfo[] toArray(){
    	DependencyStatusInfo[] statuses = new DependencyStatusInfo[dependencyStatusInfos.size()];
    	for (int i = 0; i < statuses.length; i++) {
			statuses[i] = dependencyStatusInfos.get(i);
		}
    	return statuses;
    }
}
