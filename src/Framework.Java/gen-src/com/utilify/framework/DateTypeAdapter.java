package com.utilify.framework;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Internal utility class for marshaling and un-marshaling dates between Java and XML.
 */
public class DateTypeAdapter extends XmlAdapter<String, Date> {

    /**
     * Converts an XML string to a Java <code>Date</code>.
     * @param value - the value to convert to a Date.
     * @return the converted Date.
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    public Date unmarshal(String value) {
        return (DatatypeConverter.parseDateTime(value).getTime());
    }

    /**
     * Converts Java <code>Date</code> to an XML string.
     * @param value - the date to convert to a string.
     * @return the converted string.
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    public String marshal(Date value) {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(value);
        //System.out.println(DatatypeConverter.printDateTime(cal));
		return DatatypeConverter.printDateTime(cal);
    }   
}