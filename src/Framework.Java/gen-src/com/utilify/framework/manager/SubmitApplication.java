
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ApplicationSubmissionInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appInfo" type="{http://schemas.utilify.com/2007/09/Client}ApplicationSubmissionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "appInfo"
})
@XmlRootElement(name = "SubmitApplication")
public class SubmitApplication {

    @XmlElement(nillable = true)
    protected ApplicationSubmissionInfo appInfo;

    /**
     * Gets the value of the appInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationSubmissionInfo }
     *     
     */
    public ApplicationSubmissionInfo getAppInfo() {
        return appInfo;
    }

    /**
     * Sets the value of the appInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationSubmissionInfo }
     *     
     */
    public void setAppInfo(ApplicationSubmissionInfo value) {
        this.appInfo = value;
    }

}
