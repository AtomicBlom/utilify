package com.utilify.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;

/**
 */
public class ApplicationProperties {

	private static final long serialVersionUID = -8276088805329493007L;
	private static final String defaultFilename = "utilify.framework.application.properties"; //default

	private URL managerUrl = null;
	private String filename = defaultFilename;

	private Properties props = new Properties();
	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 */
	public ApplicationProperties(){
		try {
			load(defaultFilename);
		} catch (IOException e) {
			logger.fine("Error loading properties from default file: " + defaultFilename);
			logger.fine(Helper.getFullStackTrace(e));
			setDefaults();
		}
	}
	
	private void setDefaults() {
		try {
			String baseUrl = "http://localhost:8080/Utilify.Platform.Manager/ApplicationManager";
			managerUrl = new URL(baseUrl);
		} catch (MalformedURLException e) {
			//we know the above URL is not malformed.
		}
	}
	
	/**
	 * @return the managerUrl
	 */
	public URL getManagerUrl() {
		return managerUrl;
	}

	/**
	 * @param managerUrl the managerUrl to set
	 */
	public void setManagerUrl(URL managerUrl) {
		this.managerUrl = managerUrl;
	}
	
	/**
	 * @param filename
	 * @throws IOException
	 */
	public void load(@SuppressWarnings("hiding")
			String filename) throws IOException {
		
		this.filename = filename;
		File file = new File(filename);
		logger.fine("Loading properties from " + file.getAbsolutePath() + ", file exists = " + file.exists());
		props.load(new FileInputStream(file));
		
		managerUrl = new URL(props.getProperty("managerUrl"));
	}
	
	/**
	 * @param filename
	 * @throws IOException
	 */
	public void save(@SuppressWarnings("hiding")
			String filename) throws IOException{
		
		props.setProperty("managerUrl", managerUrl.toString());
		
		props.store(new FileOutputStream(filename), "");
	}
	
	/**
	 * @throws IOException
	 */
	public void save() throws IOException{
		save(filename);
	}
}
