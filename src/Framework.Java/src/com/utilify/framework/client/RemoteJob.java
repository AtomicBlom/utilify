
package com.utilify.framework.client;

import com.utilify.framework.JobStatus;

/**
 * A <code>RemoteJob</code> is the client-side representation of a job submitted for execution to the Manager.
 */
public final class RemoteJob {

	private String id;
	private Executable job;
	private JobStatus status;
	private Executable completedJob;
	
	/**
	 * Creates an instance of the <code>RemoteJob</code> class with the specified executable job instance.
	 * @param job
	 * 	an instance of {@link Executable} that represents the code to execute remotely.
	 */
	public RemoteJob(Executable job) {
		this.job = job;
		this.id = "";
		this.status = JobStatus.UN_INITIALIZED;
	}

	//all getters and setters of this class are thread-safe and synchronized 
	//- since they are always used on different threads.
	/**
	 * Gets the orginially submitted instance of the executable job. 
	 * @return
	 * 	the submitted job instance.
	 */
	public Executable getJob(){
		Executable copy = null;
		synchronized (this.job) {
			copy = job;
		}
		return copy;
	}

	/**
	 * Gets the Id of the job.
	 * If the job does not yet have an Id, (possibly because it is still being submitted),
	 * it returns <code>null</code>.
	 * @return
	 * 	the Id of this job.
	 */
	public String getId() {
		String copy = null;
		synchronized (this.id) {
			copy = this.id;
		}
		return copy;
	}
	
	void setId(String value){
		synchronized (this.id) {
			this.id = value;
		}
	}

	/**
	 * Gets an instance of the completedjob.
	 * If the job has not yet finished executing, it returns <code>null</code>.
	 * @return
	 * 	the instance of the completed job.
	 */
	public Executable getCompletedJob() {
		Executable copy = null;
		synchronized (this.completedJob) {
			copy = completedJob;
		}
		return copy;
	}

	void setCompletedJob(Executable completedJob) {
		synchronized (this.completedJob) {
			this.completedJob = completedJob;
		}
	}
	
	/**
	 * Gets the status of the job.
	 * If the job is in the process of submission, it returns <code>null</code>.
	 * @return
	 * 	the job status.
	 */
	public JobStatus getStatus() {
		JobStatus copy = null;
		synchronized (this.status) {
			copy =  status;
		}
		return copy;
	}

	void setStatus(JobStatus status) {
		synchronized (this.status) {
			this.status = status;	
		}
	}
	
	private boolean isCancelRequested = false;
	/**
	 * @return isCancelRequested
	 */
	boolean isCancelRequested() {
		boolean isReq = false;
		synchronized (this) {
			isReq = isCancelRequested;
		}
		return isReq;
	}
	/**
	 * @param isCancelRequested
	 */
	void setCancelRequested(boolean isCancelRequested) {
		synchronized (this) {
			this.isCancelRequested = isCancelRequested;
		}
	}

	/**
	 * Blocks the current thread till this job completes remote execution.
	 * @throws InterruptedException
	 * 	if the thread is interrupted while it is blocked.
	 */
	public void waitForCompletion() throws InterruptedException {
		while (true){
			JobStatus s = getStatus();
			if (s == JobStatus.COMPLETED
					|| s == JobStatus.CANCELLED)
				break;
			Thread.sleep(1000);
		}
	}
}
