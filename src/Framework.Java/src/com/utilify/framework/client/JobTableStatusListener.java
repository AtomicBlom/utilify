
package com.utilify.framework.client;

public interface JobTableStatusListener {
	void onJobTableStatusChanged(JobTableStatusEvent event);
}
