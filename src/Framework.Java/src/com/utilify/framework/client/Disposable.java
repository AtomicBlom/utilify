
package com.utilify.framework.client;

/**
 * The interface to implement to specify that the class has expensive resources to close or dispose off.
 * The class that implements this interface declares that it could be holding references to expensive resources,
 * which can be freed by a call to the {@link #dispose()} method.
 */
public interface Disposable {
	/**
	 * Frees any resources (such as file handles, network or database connections etc. held by the implementing class.
	 */
	void dispose();
}
