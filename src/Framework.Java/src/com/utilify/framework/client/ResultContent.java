
package com.utilify.framework.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ResultContent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultContent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResultId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the content of a result produced by execution of a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultContent", propOrder = {
    "jobId",
    "resultId",
    "content"
})
public class ResultContent {

    @XmlElement(name = "JobId", required = true, nillable = true)
    private String jobId;
    @XmlElement(name = "ResultId", required = true, nillable = true)
    private String resultId;
    @XmlElement(required = true, nillable = true)
    private byte[] content;

    private ResultContent(){}
    
    //no public ctors for this class, since it is created on the server side.

	/**
     * Gets the Id of the job that produced this result.
     * @return
     * 	the job Id.
     */
    public String getJobId() {
        return jobId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setJobId(String value) {
        this.jobId = value;
    }

    /**
     * Gets the Id of this result.
     * @return
     * 	the result Id. 
     */
    public String getResultId() {
        return resultId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setResultId(String value) {
        this.resultId = value;
    }

    /**
     * Gets the data contents of this result.
     * @return
     *	the byte[] containing the data.
     */
    public byte[] getContent() {
        return content;
    }

    @SuppressWarnings("unused") //used by JAXWS
	private void setContent(byte[] value) {
        this.content = value;
    }
}
