
package com.utilify.framework.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;

/*
 * <p>Java class for DependencyContent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DependencyContent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="DependencyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the content of a dependency.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DependencyContent", propOrder = {
    "content",
    "dependencyId"
})
public class DependencyContent {

    @XmlElement(name = "Content", required = true, nillable = true)
    private byte[] content;
    @XmlElement(name = "DependencyId", required = true, nillable = true)
    private String dependencyId;

    private DependencyContent(){}
    
    /**
     * Creates an instance of the <code>DependencyContent</code> class with the specified Id and data.
     * @param dependencyId
     * 	the Id of the dependency.
     * @param fileContent
     * 	the content data of the dependency.
     */
    public DependencyContent(String dependencyId, byte[] fileContent) {
    	//todoLater: everywhere : have JAXB bound to props as opposed to fields
    	setDependencyId(dependencyId);
    	setContent(fileContent);
	}

	/**
     * Gets the data contents of this dependency.
     * 
     * @return
     *	the byte[] containing the data.
     */
    public byte[] getContent() {
        return content;
    }

    private void setContent(byte[] value) {
    	if (value == null || value.length == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": content");
        this.content = value;
    }

    /**
     * Gets the dependency Id.
     * @return
     * 	the Id of this dependency.
     */
    public String getDependencyId() {
        return dependencyId;
    }

    private void setDependencyId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": dependencyId");
        this.dependencyId = value;
    }
}
