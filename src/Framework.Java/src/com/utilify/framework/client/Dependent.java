
package com.utilify.framework.client;

/**
 * The interface to implement to declare dependencies for a job. The class that implements this interface (which should be the one that
 * implements the {@link Executable} interface that represents an executable job),
 * declares that it depends on certain files and/or resources. The list of these dependencies is retrieved 
 * by the framework during job submission, using the {@link #getDependencies()} method.
 */
public interface Dependent {
	/**
	 * Gets the list of dependencies for this job.
	 * @return
	 * 	the list of dependencies required for this job to execute.
	 */
	DependencyInfo[] getDependencies();
}
