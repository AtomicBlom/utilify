
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for JobType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CodeModule"/>
 *     &lt;enumeration value="NativeModule"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

/**
 * Represents the type of a job. Jobs can be of the following types:<br>
 * <li>CodeModule</li><br>
 * A user implemented job created by extending the Executable interface and implementing the execute method.
 * <li>NativeModule</li><br>
 * A native application or library (compiled binary) (not written in a managed language such as Java or one of the .NET platform languages).
 */
@XmlType(name = "JobType")
@XmlEnum
public enum JobType {

    /**
     * Job type for Code module.
     */
    @XmlEnumValue("CodeModule")
    CODE_MODULE("CodeModule"),
    /**
     * Job type for native module.
     */
    @XmlEnumValue("NativeModule")
    NATIVE_MODULE("NativeModule");
    private final String value;

    JobType(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>JobType</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>JobType</code> object.
     * @param v - the value from which to create the <code>JobType</code>.
     * @return the <code>JobType</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable type values.
     */
    public static JobType fromValue(String v) {
        for (JobType c: JobType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
