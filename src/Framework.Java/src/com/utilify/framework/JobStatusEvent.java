
package com.utilify.framework;

import com.utilify.framework.client.Application;
import com.utilify.framework.client.JobStatusInfo;

/**
 * Represents an job status event that occurs when the status of a job has changed from its previous value.
 * A job status event is raised by the {@link Application} class, to inform the user when the new status is discovered by the Application.
 */
public class JobStatusEvent {
	private JobStatusInfo statusInfo;
	
	/**
	 * Constructs an instance of the <code>JobStatusEvent</code> class with the specified status information.
	 * @param statusInfo - an instance of {@link JobStatusInfo} that contains the status information.
	 */
	public JobStatusEvent(JobStatusInfo statusInfo){
		this.statusInfo = statusInfo;
	}
	
	/**
	 * Gets the status information associated with this event.
	 * @return an instance of <code>JobStatusInfo</code> that contains the status information.
	 */
	public JobStatusInfo getStatusInfo(){
		return statusInfo;
	}
}
