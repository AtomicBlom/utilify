
package com.utilify.framework;

import com.utilify.framework.client.Application;

/**
 * The listener interface for receiving job submitted events. 
 * The class that is interested in processing a job submitted event implements this interface, 
 * and the object created with that class is registered with an {@link Application}, 
 * using it's {@link Application#addJobSubmittedListener} method. When the job submitted event occurs, 
 * that object's {@link #onJobSubmitted} method is invoked. 
 */
public interface JobSubmittedListener {
	/**
	 * Invoked when a job is submitted. 
	 * @param event - the event object the contains the submitted job information.
	 */
	void onJobSubmitted(JobSubmittedEvent event);
}
