It seems we can set the affinity for a process running pure-managed code in C#.

But to set the affinity mask for a thread, we need to use the kernel32.dll.

reference:
(http://forums.microsoft.com/msdn/showpost.aspx?postid=2701577&siteid=1&mode=1&sb=0&d=1&at=7&ft=11&tf=0&pageid=1)

Also, it is not advisable to mess with the Windows OS scheduler, especially when we know that there will be other processes/threads running on the system at the same time anyway.

It is best to let the OS schedule the threads, and our Executor will just report the number of CPUs/cores to the Manager, and our meta-scheduler in the Manager can decide on how many concurrent tasks/jobs to run on the Executor at the same time.